#include "Application.h"
#include "ETH_hal.h"
#include "PHY_ksz8081.h"
#include "ETH_driver.h"
#include "NetworkBufferManagement.h"
#include "NetworkInterface.h"


/** Alignment macro */
#define __ALIGN_MASK( x, mask )                    ( ( ( x ) + ( mask ) )& ~( mask ) )
#define ALIGN32( x, a )                            __ALIGN_MASK( x, ((uint32)(a) - 1 ))

/** Parameters of Ethernet buffers. */
/* WORKAROUND: Need to create one more Rx Buffer than defined by Processor Expert.
 * There appears to be a problem LDD_Queue macros.  By creating one more Rx buffer
 * helps get around the problem. */
#define ENET_RXBD_NUM                           (ETH0_RX_QUEUE_SIZE+1u)
#define ENET_TXBD_NUM                           (ETH0_TX_QUEUE_SIZE)
#define ENET_RXBUFF_SIZE                        (kEnetMaxFrameSize)
#define ENET_TXBUFF_SIZE                        (kEnetMaxFrameSize)
#define ENET_RX_BUFFER_ALIGNMENT                (16)
#define ENET_TX_BUFFER_ALIGNMENT                (16)

/**---------------------------------------------------------------------------------------------------------------------
 * @brief   Kinetis requires 16-byte align for its buffers. Although it's a hardware requirement and
 *           has a significance during MAC buffer init, FreeRTOS IP stack will also be happy
 *           with this alignment.
 * ---------------------------------------------------------------------------------------------------------------------
 */
#define ENET_BD_ALIGNMENT                       (16)
#define ENET_RXBuffSizeAlign(n)                 ALIGN32(n, ENET_RX_BUFFER_ALIGNMENT)
#define ENET_TXBuffSizeAlign(n)                 ALIGN32(n, ENET_TX_BUFFER_ALIGNMENT)

#if FSL_FEATURE_ENET_SUPPORT_PTP
#define ENET_PTP_TXTS_RING_LEN                  (25)
#define ENET_PTP_RXTS_RING_LEN                  (25)
#endif
#define ENET_CABLE_DETECT_TO                    300     /** [ms] time for detecting whether the cable is plugged in */

#define BUFFER_SIZE                             ( ipTOTAL_ETHERNET_FRAME_SIZE + ipBUFFER_PADDING ) // Min size of network buffers

static LDD_TUserData        *gpLocalEthernetPointer; // A pointer to this type specifies that the user- or RTOS-specific data will be passed as an event or callback parameter.
static LDD_TDeviceData      *gpEth0LowLevelDeviceState;
static LDD_ETH_TBufferDesc  gxFrameDataToSend;
static uint8                gu8LastTxError = ERR_OK;
static uint8                gu8LastRxError = ERR_OK;

typedef struct
{
    uint8* buff;
} ENET_bufferContainer_t;

static ENET_bufferContainer_t ENET_rxBuffContainer[ENET_RXBD_NUM];
volatile enet_bd_struct_t __attribute__((aligned(16))) ENET_rxBufferDescriptor[ENET_RXBD_NUM * sizeof(enet_bd_struct_t)];
volatile enet_bd_struct_t __attribute__((aligned(16))) ENET_txBufferDescriptor[ENET_TXBD_NUM * sizeof(enet_bd_struct_t)];
volatile uint8 __attribute__((aligned(32))) ENET_rxBuffer[ENET_RXBD_NUM][ENET_RXBuffSizeAlign(ENET_RXBUFF_SIZE)];
volatile uint8 __attribute__((aligned(32))) ENET_txBuffer[ENET_TXBD_NUM][ENET_TXBuffSizeAlign(ENET_TXBUFF_SIZE)];

//volatile uint8 __attribute__((aligned(32))) ENET_networkBuffer[ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS][ALIGN32( BUFFER_SIZE, ENET_BD_ALIGNMENT )];

/** Ethernet frame header. */
struct enet_header {
    uint8_t dst_mac[6];
    uint8_t src_mac[6];
    uint16_t type;
} __attribute__((packed));

/** ARP request header */
struct arp_header {
    struct enet_header eth;
    uint16_t htype;
    uint16_t ptype;
    uint8_t hlen;
    uint8_t plen;
    uint16_t oper;
    uint8_t sender_mac[6];
    uint8_t sender_ip[4];
    uint8_t trg_mac[6];
    uint8_t trg_ip[4];
} __attribute__((packed));

/** Variable size, depends on IHL value! This is the shortest IPv4 header. */
struct ipv4_header {
    uint8_t byte0;                              /**< version + IHL */
    uint8_t tos;
    uint16_t length;
    uint16_t id;
    uint8_t reserved1[3];
    uint8_t protocol;
    uint8_t reserved2[2];
    uint8_t src_ip[4];
    uint8_t dst_ip[4];
} __attribute__((packed));
/** Component state. */
struct network_interface_state {
    /* these must be persistent, since the ethernet driver just stores
    a pointer to them during init (instead of copying) */
    enet_dev_if_t itf;
    enet_user_config_t cfg;
    enet_mac_config_t mac_cfg;
    enet_buff_config_t buff_cfg;
    enet_config_rmii_t rmii_cfg;

    /* ethernet driver buffers */
    uint8 *rxBdPtr;
    uint8 *txBdPtr;
    uint8 *rxBuffer;
    uint8 *txBuffer;
#if FSL_FEATURE_ENET_SUPPORT_PTP
    enet_mac_ptp_ts_data_t *ptpTsRxData;
    enet_mac_ptp_ts_data_t *ptpTsTxData;
#endif

    /* temporary RX packet store */
    SemaphoreHandle_t rxq_run;                  /**< counting semaphore */
    // ARH - todo - is this the right max size?
    uint8_t rxq[BUFFER_SIZE + 32];              /**< receive queue (from MAC) */
    uint16_t rxq_len;                           /**< receive queue length */

    uint8_t init_done : 1;                      /**< all initialization are done */
};
static volatile struct network_interface_state nstate = {
    .init_done = 0,
};
static uint32_t enet_buffer_init(enet_dev_if_t *itf, enet_buff_config_t *buff);
#if 0
static void     enet_frame_dump(uint8_t *data, uint32_t data_len, char *text);
#endif
static uint32_t enet_rx_cb(void *enet_ptr, enet_mac_packet_buffer_t *packet);
static void     enet_rx_cb_deferred(void *param);

// endianess reverse functions
uint32 Reverse32(uint32 input);
uint16 Reverse16(uint16 input);



/**---------------------------------------------------------------------------------------------------------------------
 * @func        xNetworkInterfaceInitialise
 * @brief       Ethernet peripheral initialization.
 *
 * @desc        If returns with anything else than pdPASS, the TCP/IP stack doesn't give up
 *               and calls this function further.
 *
 * @return      BaseType_t
 * ---------------------------------------------------------------------------------------------------------------------
 */
BaseType_t xNetworkInterfaceInitialise(void)
{
    uint32_t result;
    enet_phy_speed_t physpeed;
    enet_phy_duplex_t phyduplex;
    bool linkstatus;

    nstate.itf.enetNetifcall = &enet_rx_cb;
    /* WARNING The OSA mutex itf.enetContextSync is used only by RTCS TCP/IP stack
    in KSDK-1.2.0, so we don't have to initialize. */
    /* OSA_MutexCreate(&itf.enetContextSync); */
    nstate.cfg.macCfgPtr = (enet_mac_config_t *)&nstate.mac_cfg;
    nstate.cfg.buffCfgPtr = (enet_buff_config_t *)&nstate.buff_cfg;

    nstate.mac_cfg.macMode         = kEnetMacNormalMode;
    /* ipLOCAL_MAC_ADDRESS is filled by FreeRTOS_IPInit(...) */
    nstate.mac_cfg.macAddr         = (uint8_t *)ipLOCAL_MAC_ADDRESS;
    nstate.mac_cfg.rmiiCfgPtr      = (enet_config_rmii_t *)&nstate.rmii_cfg;
    /* essential to work */
    nstate.mac_cfg.macCtlConfigure  = kEnetMacEnhancedEnable;
    /* strip received frame CRC */
    nstate.mac_cfg.macCtlConfigure |= kEnetRxCrcFwdEnable;
    /* auto add TX CRC */
    nstate.mac_cfg.macCtlConfigure |= kEnetTxCrcBdEnable;
    /* insert src MAC address */
    nstate.mac_cfg.macCtlConfigure |= kEnetMacAddrInsert;

    /* allocating RX/TX buffers */
    if (enet_buffer_init((enet_dev_if_t *)&nstate.itf, (enet_buff_config_t *)&nstate.buff_cfg) != kStatus_ENET_Success)
        return pdFAIL;

    /* This doesn't really seem to have any effect in case the PHY is capable of
    auto negotiation. */
    nstate.rmii_cfg.mode             = kEnetCfgRmii;
    nstate.rmii_cfg.speed            = kEnetCfgSpeed100M;
    nstate.rmii_cfg.duplex           = kEnetCfgFullDuplex;
    nstate.rmii_cfg.isRxOnTxDisabled = FALSE;
    nstate.rmii_cfg.isLoopEnabled    = FALSE;

	// Initialize the Ethernet Controller
    gpEth0LowLevelDeviceState = ETH0_Init( gpLocalEthernetPointer );
    /* TODO: FIx me */
    //nstate.itf.bdContext.rxBdCurPtr = nstate.buff_cfg.rxBufferAlign;
    nstate.itf.bdContext.rxBdCurPtr = (enet_bd_struct_t *)ENET_RDSR;
    nstate.itf.bdContext.rxBdDirtyPtr = nstate.itf.bdContext.rxBdCurPtr;
    nstate.itf.bdContext.isRxBdFull = FALSE;
    nstate.itf.bdContext.rxBdBasePtr  = nstate.itf.bdContext.rxBdCurPtr;
#if 0 //RB
    nstate.itf.bdContext.rxBdBasePtr  = nstate.buff_cfg.rxBdPtrAlign;
    nstate.itf.bdContext.rxBdCurPtr   = nstate.buff_cfg.rxBdPtrAlign;
    nstate.itf.bdContext.rxBdDirtyPtr = nstate.buff_cfg.rxBdPtrAlign;
    nstate.itf.bdContext.txBdBasePtr  = nstate.buff_cfg.txBdPtrAlign;
    nstate.itf.bdContext.txBdCurPtr   = nstate.buff_cfg.txBdPtrAlign;
    nstate.itf.bdContext.txBdDirtyPtr = nstate.buff_cfg.txBdPtrAlign;
    nstate.itf.bdContext.rxBuffSizeAlign = nstate.buff_cfg.rxBuffSizeAlign;
    nstate.itf.bdContext.txBuffSizeAlign = nstate.buff_cfg.txBuffSizeAlign;

    statusMask |= ENET_GET_MAX_FRAME_LEN_MASK;
    ENET_HAL_GetStatus(base, statusMask, &curStatus);
    enetIfPtr->maxFrameSize = curStatus.maxFrameLen;
    /* Extend buffer for data buffer update*/
    if(buffCfgPtr->extRxBuffQue != NULL)
    {
        uint16_t counter = 0;
        enetIfPtr->bdContext.extRxBuffQue = NULL;
        for(counter = 0; counter < buffCfgPtr->extRxBuffNum; counter++)
        {
            enet_mac_enqueue_buffer((void **)&enetIfPtr->bdContext.extRxBuffQue,
                (buffCfgPtr->extRxBuffQue + counter * buffCfgPtr->rxBuffSizeAlign));
        }
        enetIfPtr->bdContext.extRxBuffNum = buffCfgPtr->extRxBuffNum;
    }
#endif


    if( NULL == gpEth0LowLevelDeviceState )
    {
        CPU_SystemReset();
	}

    if( ERR_OK != ETH0_SetMACAddress( gpEth0LowLevelDeviceState, (uint8 *)pEthernetGetAdapterMAC() ) ) // Retrieve a pointer to the MAC addr and set it in the driver
    {
        CPU_SystemReset();
    }
    /* Initialize PHY*/
    PHY_DRV_Init(0, 0, FALSE);

    /*get negotiation results and reconfigure MAC speed and duplex according to phy*/
    result = PHY_DRV_GetLinkStatus(0, 0, &linkstatus);
    if(result == kStatus_ENET_Success)
    {
        if(linkstatus == TRUE)
        {
            result = PHY_DRV_GetLinkSpeed(0, 0,&physpeed);
            if(result == kStatus_ENET_Success)
            {
               result = PHY_DRV_GetLinkDuplex(0, 0,&phyduplex);
               if(result == kStatus_ENET_Success)
               {
                   if (physpeed == kEnetSpeed10M)
                   {
                       nstate.rmii_cfg.speed = kEnetCfgSpeed10M;
                   }
                   else
                   {
                       nstate.rmii_cfg.speed = kEnetCfgSpeed100M;
                   }
                   if (phyduplex == kEnetFullDuplex)
                   {
                       nstate.rmii_cfg.duplex = kEnetCfgFullDuplex;
                   }
                   else
                   {
                       nstate.rmii_cfg.duplex = kEnetCfgHalfDuplex;
                   }
                   ENET_HAL_SetRMIIMode( (ENET_MemMapPtr)(ENET_BASE_PTR), (enet_config_rmii_t *)&nstate.rmii_cfg);
               }
            }
        }
    }

    /* counting semaphore to control when the deferred RX task should run */
    nstate.rxq_run = xSemaphoreCreateCounting(ENET_RXBD_NUM + 1, 0);
    if (nstate.rxq_run == NULL)
    {
        CPU_SystemReset();
	}

    /* create deferred task to process RX packets */
	if(ipconfigIP_TASK_PRIORITY <= 0)
	{
        CPU_SystemReset();
	}
	
	result = xTaskCreate(enet_rx_cb_deferred, "rx_cb_deferred", configMINIMAL_STACK_SIZE, NULL, ipconfigIP_TASK_PRIORITY - 1, NULL);
	
    if (result != pdPASS)
    {
        CPU_SystemReset();
	}

    nstate.init_done = 1;

    vAppEthEnableRxBuff();

    return pdPASS;
}

/**---------------------------------------------------------------------------------------------------------------------
 * @func        xNetworkInterfaceOutput
 *
 * @brief       Sends out an Ethernet frame.
 *
 * @desc        Although this function has a return type, the TCP/IP stack never looks at the
 *               return value.
 *
 * @return      BaseType_t
 * ---------------------------------------------------------------------------------------------------------------------
 */
BaseType_t xNetworkInterfaceOutput( NetworkBufferDescriptor_t *const pxNetworkBuffer, BaseType_t xReleaseAfterSend )
{
    static uint8 ENET_txBufferIdx = 0;
    BaseType_t retStatus = pdFAIL;
    uint8 u8Result = 0;

    if (nstate.init_done != 0)
    {
        nstate.txBuffer = (uint8 *)ENET_txBuffer[ENET_txBufferIdx];
        ENET_txBufferIdx++;
        if(ENET_txBufferIdx >= ENET_TXBD_NUM)
        {
            ENET_txBufferIdx = 0U;
        }
        /* copy to ethernet driver's buffer from TCP/IP network buffer and transmit */
        memcpy(nstate.txBuffer, pxNetworkBuffer->pucEthernetBuffer, pxNetworkBuffer->xDataLength);
        gxFrameDataToSend.Size = pxNetworkBuffer->xDataLength;
        gxFrameDataToSend.DataPtr = nstate.txBuffer;


        u8Result = ETH0_SendFrame( gpEth0LowLevelDeviceState, &gxFrameDataToSend, 1 );

        /**---------------------------------------------------------------------------------------------------------------------
         *
         *                    INFORMATION FOR PEx ETH0SendFrame function:
         *
         * @func    LDD_TError ETH0_SendFrame( DeviceDataPtr, BufferDescListPtr, BufferDescCount )
         *
         * @desc    Initiates a frame transmission. Frame contents are defined
         *           by the specified list of buffer descriptors. Frames are
         *           internally queued. The end of the frame transmission is
         *           signaled by the OnFrameTransmitted event.
         *
         * @param   DeviceDataPtr       Pointer to low-level device data structure
         *
         * @param   BufferDescListPtr   Pointer to list of buffer descriptors defining
         *                               contents of one frame.
         *
         * @param   BufferDescCount     Number of buffer descriptors in the list.
         *
         * @return  LDD_TError          Error code, possible values below:
         * @retval  ERR_OK              Success
         * @retval  ERR_SPEED           The component does not work in the active clock configuration
         * @retval  ERR_DISABLED        The component is disabled
         * @retval  ERR_QFULL           Frame queue is full
         * @retval  ERR_PARAM_ADDRESS   Invalid address alignment (one or more buffer address from the
         *                               list of buffer descriptors is misaligned )
         * @retval  ERR_PARAM_LENGTH    Invalid frame length (sum of buffer sizes from the list of buffer
         *                               descriptors is greater than maximum frame length)
         * ---------------------------------------------------------------------------------------------------------------------
         */


        if( ERR_OK == u8Result )
        {
            // Success!
            retStatus = pdTRUE;
        }
        else
        {
            gu8LastTxError = u8Result; // Set the module-scope error variable for debugging
        }

        iptraceNETWORK_INTERFACE_TRANSMIT();
    }

    if( xReleaseAfterSend )
    {
        vReleaseNetworkBufferAndDescriptor( pxNetworkBuffer );
    }
    return retStatus;
}


/**---------------------------------------------------------------------------------------------------------------------
 * @func        vNetworkInterfaceAllocateRAMToBuffers
 *
 * @brief       Allocates memory for buffers.
 *
 * @param[in]   NetworkBufferDescriptor_t xDescriptorArray[] An array of Buffer Descriptors to be allocated
 *
 *
 * @desc        If using BufferAllocation_1.c (see Makefile), then the network buffers must be
 *               allocated somewhere in the initialization phase. This function is called
 *               sooner than @c xNetworkInterfaceInitialise, so the buffers must already be
 *               ready.
 *
 * @return      None
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vNetworkInterfaceAllocateRAMToBuffers( NetworkBufferDescriptor_t xDescriptorArray[ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS] )
{
    void *ptr;
    uint8 *pu8Start;
    uint32 i;
    uint32 size;

    if (nstate.init_done != 0)
    {
        return;
    }

    memset((void *)&nstate, 0, sizeof(nstate));

#if 0 //RB - TODO: Try to get this to work
    for( i = 0; i < ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS; i++ )
    {
        // pucEthernetBuffer points to the start of an Ethernet frame
        xDescriptorArray[i].pucEthernetBuffer = ENET_networkBuffer[i];
    }
#else

    /* Network buffers of the IP stack. Allocating one huge memory
     block and splitting it internally up. It'd be a good idea to
     allocate separate buffer for each network buffer, since this
     way memdbg could trace each buffer for overwrite, but this
     would cause some extra memory use. Assuming the TCP/IP stack
     is mature enough to avoid memory overwrite... */

    /* all buffers must begin on an aligned address and
     all buffers must have an aligned size */
    size = ALIGN32( BUFFER_SIZE, ENET_BD_ALIGNMENT );
    size *= ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS;
    size = ALIGN32( size, ENET_BD_ALIGNMENT );
    /* some extra space is needed, since malloc may return with a non-correctly
     aligned base address */
    size += ENET_BD_ALIGNMENT;

    /* allocate and align base address */
    ptr = pvPortMalloc( size );

    if( ptr == NULL )
    {
        CPU_SystemReset();
    }

    pu8Start = (uint8 *)ALIGN32( (uint32_t )ptr, ENET_BD_ALIGNMENT );

    for( i = 0; i < ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS; i++ )
    {
        // pucEthernetBuffer points to the start of an Ethernet frame
        xDescriptorArray[i].pucEthernetBuffer = pu8Start + ipBUFFER_PADDING;
        *( (uint32_t *)pu8Start ) = (uint32_t)&xDescriptorArray[i];
        pu8Start += ALIGN32( BUFFER_SIZE, ENET_BD_ALIGNMENT );
    }
#endif
}


void vAppEthEnableRxBuff(void)
{
    uint8 idx;

    /* Initialize and enable Frame reception  */
    for(idx = 0; idx < ENET_RXBD_NUM; idx++)
    {
        ENET_rxBuffContainer[idx].buff = (uint8 *)ENET_rxBuffer[idx];
    }

    /* WORKAROUND: Send in one less bufferCounts than defined.  There appears to be a problem
     * with the LDD_QUEUE macros.  If the correct bufferCount is passed in, this function will
     * not setup the RxBuffer Descriptors.  LDD_QUEUE_GET_FREE_COUNT will fail.  The last descriptor
     * will get setup during the first rx ISR. */
    ETH0_ReceiveFrame(gpEth0LowLevelDeviceState, &ENET_rxBuffContainer, ENET_RXBD_NUM-1u);
}

void vAppEthernetModuleRx_ISR(LDD_TUserData *pUserData, uint16 u16FragCount, uint16 u16Length )
{
    /* using enetIfPtr to try to keep this code similar to ENET_DRV_ReceiveData*/
    enet_dev_if_t * enetIfPtr = (enet_dev_if_t *)&nstate.itf;

    volatile enet_bd_struct_t *curBd;
    uint16_t bdNumTotal = 0, lenTotal = 0;
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    enet_mac_packet_buffer_t packetBuffer[kEnetMaxFrameBdNumbers] = {{0}};
    static uint8 ETH_rxQueueIdx = 0;
    gu8LastRxError = ERR_OK;

    /* Check input parameters*/
    if ((!enetIfPtr) || (!enetIfPtr->bdContext.rxBdCurPtr))
    {
        gu8LastRxError = kStatus_ENET_InvalidInput;
        return;
    }

    /* Check the current buffer descriptor address*/
    curBd = enetIfPtr->bdContext.rxBdCurPtr;
    /* Return if the current buffer descriptor is empty*/
    mask |= (ENET_RX_BD_EMPTY_FLAG_MASK | ENET_RX_BD_TRUNC_FLAG_MASK | ENET_RX_BD_LAST_FLAG_MASK | ENET_BD_LEN_MASK);
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
    //while(!(bdAttr.flags & ENET_RX_BD_EMPTY_FLAG))
    if(!(bdAttr.flags & ENET_RX_BD_EMPTY_FLAG))
    {
        /* Check if receive buffer is full*/
        if(enetIfPtr->bdContext.isRxBdFull)
        {
#if ENET_ENABLE_DETAIL_STATS
            enetIfPtr->stats.statsRxDiscard++;
#endif
            ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
            gu8LastRxError = kStatus_ENET_RxBdFull;
            return;
        }

        /* Increase current buffer descriptor to the next one*/
        enetIfPtr->bdContext.rxBdCurPtr =
            ENET_DRV_IncrRxBuffDescripIndex(enetIfPtr, enetIfPtr->bdContext.rxBdCurPtr);
        /* Check if the buffer is full*/
        if (enetIfPtr->bdContext.rxBdCurPtr == enetIfPtr->bdContext.rxBdDirtyPtr)
        {
            enetIfPtr->bdContext.isRxBdFull = TRUE;
        }

         /* Discard packets with truncate error*/
        if (bdAttr.flags & ENET_RX_BD_TRUNC_FLAG)
        {
#if ENET_ENABLE_DETAIL_STATS
            enetIfPtr->stats.statsRxTruncate++;
            enetIfPtr->stats.statsRxDiscard++;
#endif
            ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
            gu8LastRxError = kStatus_ENET_RxbdTrunc;
            return;
        }

       if (bdAttr.flags & ENET_RX_BD_LAST_FLAG)
       {
           /* The last bd in the frame check the status of the received frame*/
           if (ENET_DRV_RxErrorStats(enetIfPtr, curBd))
           {
               ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
               gu8LastRxError = kStatus_ENET_RxbdError;
               return;
           }
           else
           {
                packetBuffer[bdNumTotal].data = ENET_HAL_GetBuffDescripData(curBd);
                packetBuffer[bdNumTotal].length = bdAttr.bdLen - lenTotal;
                /* Crc length check */
                if(enetIfPtr->isRxCrcFwdEnable)
                {
                    packetBuffer[bdNumTotal].length -= kEnetFrameFcsLen;
                }
                packetBuffer[bdNumTotal].next = NULL;
#if FSL_FEATURE_ENET_SUPPORT_PTP
                ENET_DRV_GetRxTs(&enetIfPtr->privatePtp,
                    packetBuffer[0].data, curBd);
#endif
                if(enetIfPtr->enetNetifcall)
                {
                    enetIfPtr->enetNetifcall(enetIfPtr, &packetBuffer[0]);
                }

               /* Update receive buffer descriptor*/
               ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
               bdNumTotal = 0;
           }
        }
        else
        {
            packetBuffer[bdNumTotal].data = ENET_HAL_GetBuffDescripData(curBd);
            packetBuffer[bdNumTotal].length = enetIfPtr->bdContext.rxBuffSizeAlign;
            lenTotal += packetBuffer[bdNumTotal].length;
            packetBuffer[bdNumTotal].next = &packetBuffer[bdNumTotal + 1];
            bdNumTotal ++;
            /* Check a frame with total bd numbers */
            if (bdNumTotal == kEnetMaxFrameBdNumbers)
            {
#if ENET_ENABLE_DETAIL_STATS
                enetIfPtr->stats.statsRxDiscard++;
#endif
                ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
                gu8LastRxError = kStatus_ENET_SmallRxBuffSize;
                return;
            }
        }

        /* Check the current buffer descriptor address*/
        curBd = enetIfPtr->bdContext.rxBdCurPtr;
        ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);


        ETH0_ReceiveFrame(gpEth0LowLevelDeviceState, &ENET_rxBuffContainer[ETH_rxQueueIdx], u16FragCount);
        ETH_rxQueueIdx += u16FragCount;
        if(ETH_rxQueueIdx >= ENET_RXBD_NUM)
        {
            ETH_rxQueueIdx = 0;
        }
    }

    return;
}

/******************************************************************************
 * Static functions.
 ******************************************************************************/
/**
 * Taken from ethernetif.c:ENET_buffer_init(...) (lwip port in KSDK).
 */
static uint32_t enet_buffer_init(enet_dev_if_t *itf, enet_buff_config_t *buff)
{
    uint32_t rxBufferSizeAlign = 0;
    uint32_t txBufferSizeAlign = 0;
    uint8_t  *rxBufferAlign;
    uint8_t  *txBufferAlign;
    volatile enet_bd_struct_t *rxBdPtrAlign;
    volatile enet_bd_struct_t *txBdPtrAlign;

    /* Check input parameter*/
    if ((!itf) || (!buff))
        return kStatus_ENET_InvalidInput;

    /* Allocate ENET transmit buffer descriptors*/
    nstate.txBdPtr = (uint8_t *)ENET_txBufferDescriptor;
    txBdPtrAlign = (volatile enet_bd_struct_t *)ENET_txBufferDescriptor;

    /* Allocate ENET receive buffer descriptors*/
    nstate.rxBdPtr = (uint8_t *)ENET_rxBufferDescriptor;
    rxBdPtrAlign = (volatile enet_bd_struct_t *)ENET_rxBufferDescriptor;

    /* Allocate the transmit and receive data buffers*/
    nstate.rxBuffer = (uint8_t *)ENET_rxBuffer;
    rxBufferAlign = (uint8_t *)ENET_ALIGN((uint32_t)nstate.rxBuffer, ENET_RX_BUFFER_ALIGNMENT);
    nstate.txBuffer = (uint8_t *)ENET_txBuffer[0];
    txBufferAlign = (uint8_t *)ENET_ALIGN((uint32_t)nstate.txBuffer[0], ENET_TX_BUFFER_ALIGNMENT);

#if FSL_FEATURE_ENET_SUPPORT_PTP
    nstate.ptpTsRxData = (enet_mac_ptp_ts_data_t *)pvPortMallocZero(sizeof (enet_mac_ptp_ts_data_t) * ENET_PTP_RXTS_RING_LEN);
    if (nstate.ptpTsRxData == NULL)
    {
        CPU_SystemReset();
	}
    nstate.ptpTsTxData = (enet_mac_ptp_ts_data_t *)pvPortMallocZero(sizeof (enet_mac_ptp_ts_data_t) * ENET_PTP_TXTS_RING_LEN);
    if (nstate.ptpTsTxData == NULL)
    {
        CPU_SystemReset();
	}
    buff->ptpTsRxDataPtr = nstate.ptpTsRxData;
    buff->ptpTsRxBuffNum = ENET_PTP_RXTS_RING_LEN;
    buff->ptpTsTxDataPtr = nstate.ptpTsTxData;
    buff->ptpTsTxBuffNum = ENET_PTP_TXTS_RING_LEN;
#endif

    buff->extRxBuffQue = NULL;
    buff->extRxBuffNum = 0;

    buff->rxBdNumber      = ENET_RXBD_NUM;
    buff->rxBdPtrAlign    = rxBdPtrAlign;
    buff->rxBufferAlign   = rxBufferAlign;
    buff->rxBuffSizeAlign = rxBufferSizeAlign;
    buff->txBdNumber      = ENET_TXBD_NUM;
    buff->txBdPtrAlign    = txBdPtrAlign;
    buff->txBufferAlign   = txBufferAlign;
    buff->txBuffSizeAlign = txBufferSizeAlign;
    return kStatus_ENET_Success;
}

/**
 * Callback function for received ethernet frames.
 */
static uint32_t enet_rx_cb(void *enet_ptr, enet_mac_packet_buffer_t *packet)
{
    BaseType_t hi_pri = pdFALSE;

    if (nstate.init_done == 0)
        return kStatus_ENET_Success;
    if (packet->length == 0)
        return kStatus_ENET_Success;

    /* Save ethernet packet into temporary queue, since this function is in
    IRQ context and the packet can't be directly passed to the IP stack.
    The deferred task will pass the packet to the IP stack. */
	
	// ARH - todo - update this size when we fix it above
	if(packet->length > (BUFFER_SIZE + 32))
	{
		// this message is somehow bigger than the buffer it is intended for
        CPU_SystemReset();
	}
	
	nstate.rxq_len = packet->length;
	memcpy((uint8 *)nstate.rxq, packet->data, packet->length);

    /* wake up deferred task */
    xSemaphoreGiveFromISR(nstate.rxq_run, &hi_pri);

    portEND_SWITCHING_ISR(hi_pri);
    return kStatus_ENET_Success;
}

/**
 * Ethernet RX deferred processing.
 */
static void enet_rx_cb_deferred(void *param)
{
    IPStackEvent_t event;
    NetworkBufferDescriptor_t *net_buff;

    for (;;)
    {
        if (xSemaphoreTake(nstate.rxq_run, portMAX_DELAY) != pdPASS)
            continue;

        /* obtain network buffer */
        net_buff = pxGetNetworkBufferWithDescriptor(nstate.rxq_len, 0);
        if (net_buff != NULL)
        {
            /* does the stack need it at all? */
            if (eConsiderFrameForProcessing((uint8_t *)nstate.rxq) == eProcessBuffer)
            {
                /* send the ethernet frame as an event to the stack */
                memcpy(net_buff->pucEthernetBuffer, (uint8_t *)nstate.rxq, nstate.rxq_len);
                event.eEventType = eNetworkRxEvent;
                event.pvData     = (void *)net_buff;
                if (xSendEventStructToIPTask(&event, 0) != pdPASS)
                {
                    vReleaseNetworkBufferAndDescriptor(net_buff);
                    iptraceETHERNET_RX_EVENT_LOST();
                }
                else
                {
                    iptraceNETWORK_INTERFACE_RECEIVE();
                }
            }
            else
            {
                vReleaseNetworkBufferAndDescriptor(net_buff);
            }
        }
        else
        {
            iptraceETHERNET_RX_EVENT_LOST();
        }
    }
}


uint32 Reverse32(uint32 input)
{
    uint32 ret = 0;

    ret = (input & 0xFF000000) >> 24;
    ret |= (input & 0x00FF0000) >> 8;
    ret |= (input & 0x0000FF00) << 8;
    ret |= (input & 0x000000FF) << 24;

    return ret;
}

uint16 Reverse16(uint16 input)
{
    uint16 ret = 0;

    ret = (input & 0xFF00) >> 8;
    ret |= (input & 0x00FF) << 8;

    return ret;
}
