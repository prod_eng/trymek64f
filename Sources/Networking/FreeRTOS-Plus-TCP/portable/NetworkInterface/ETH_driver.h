/*
 * Copyright (c) 2013 - 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __FSL_ENET_DRIVER_H__
#define __FSL_ENET_DRIVER_H__

#include <stdint.h>
#include "ETH_hal.h"
/*! 
 * @addtogroup enet_driver
 * @{
 */

/*******************************************************************************
 * Variables
 ******************************************************************************/
/*! @brief Array for ENET module register base address. */
extern ENET_MemMapPtr const g_enetBase[];

/*******************************************************************************
 * Definitions

 ******************************************************************************/
/*! @brief Defines the approach: ENET interrupt handler receive */
#ifndef ENET_RECEIVE_ALL_INTERRUPT
#define ENET_RECEIVE_ALL_INTERRUPT  1
#endif

/*! @brief Defines the statistic enable macro.*/
#ifndef ENET_ENABLE_DETAIL_STATS
#define ENET_ENABLE_DETAIL_STATS    1
#endif



/*! @brief Defines the multicast group structure for the ENET device. */
typedef struct ENETMulticastGroup
{
    uint8_t groupAdddr[kEnetMacAddrLen];   /*!< Multicast group address*/
    uint32_t hash;                 /*!< Hash value of the multicast group address*/
    struct ENETMulticastGroup *next; /*!< Pointer of the next group structure*/
    struct ENETMulticastGroup *prv;  /*!< Pointer of the previous structure*/
} enet_multicast_group_t;


/*! @brief Defines the ENET header structure. */
typedef struct ENETEthernetHeader
{
    uint8_t destAddr[kEnetMacAddrLen];  /*!< Destination address */
    uint8_t sourceAddr[kEnetMacAddrLen];/*!< Source address*/
    uint16_t type;         /*!< Protocol type*/
} enet_ethernet_header_t;

/*! @brief Defines the ENET VLAN frame header structure. */
typedef struct ENET8021vlanHeader
{
    uint8_t destAddr[kEnetMacAddrLen];  /*!< Destination address */
    uint8_t sourceAddr[kEnetMacAddrLen];/*!< Source address*/
    uint16_t tpidtag;      /*!< ENET 8021tag header tag region*/
    uint16_t othertag;     /*!< ENET 8021tag header type region*/
    uint16_t type;         /*!< Protocol type*/
} enet_8021vlan_header_t;

/*! @brief Defines the structure for ENET buffer descriptors status.*/
typedef struct ENETBuffDescripContext
{
    volatile enet_bd_struct_t * rxBdBasePtr;   /*!< Receive buffer descriptor base address pointer*/
    volatile enet_bd_struct_t * rxBdCurPtr;    /*!< Current receive buffer descriptor pointer*/
    volatile enet_bd_struct_t * rxBdDirtyPtr;  /*!< Receive dirty buffer descriptor*/
    volatile enet_bd_struct_t * txBdBasePtr;   /*!< Transmit buffer descriptor base address pointer*/
    volatile enet_bd_struct_t * txBdCurPtr;    /*!< Current transmit buffer descriptor pointer*/
    volatile enet_bd_struct_t * txBdDirtyPtr;  /*!< Last cleaned transmit buffer descriptor pointer*/
    bool  isTxBdFull;         /*!< Transmit buffer descriptor full*/
    bool  isRxBdFull;         /*!< Receive buffer descriptor full*/
    uint32_t rxBuffSizeAlign;      /*!< Receive buffer size alignment*/
    uint32_t txBuffSizeAlign;    /*!< Transmit buffer size alignment */
    uint8_t *extRxBuffQue; /*!< Extended Rx data buffer queue to update the data buff
                                 in the receive buffer descriptor*/
    uint8_t extRxBuffNum;        /*!< extended data buffer number */
} enet_buff_descrip_context_t;

/*! @brief Defines the ENET packets statistic structure.*/
typedef struct ENETMacStats
{
    uint32_t statsRxTotal;   /*!< Total number of receive packets*/
    uint32_t statsTxTotal;   /*!< Total number of transmit packets*/
#if ENET_ENABLE_DETAIL_STATS
    uint32_t statsRxMissed;  /*!< Total number of receive packets*/
    uint32_t statsRxDiscard; /*!< Receive discarded with error */
    uint32_t statsRxError;   /*!< Receive discarded with error packets*/
    uint32_t statsTxMissed;  /*!< Transmit missed*/
    uint32_t statsTxDiscard; /*!< Transmit discarded with error */
    uint32_t statsTxError;   /*!< Transmit error*/
    uint32_t statsRxAlign;   /*!< Receive non-octet alignment*/
    uint32_t statsRxFcs;     /*!< Receive CRC error*/
    uint32_t statsRxTruncate;/*!< Receive truncate*/
    uint32_t statsRxLengthGreater;  /*!< Receive length greater than RCR[MAX_FL] */
    uint32_t statsRxCollision;      /*!< Receive collision*/
    uint32_t statsRxOverRun;        /*!< Receive over run*/
    uint32_t statsTxOverFlow;       /*!< Transmit overflow*/
    uint32_t statsTxLateCollision;  /*!< Transmit late collision*/
    uint32_t statsTxExcessCollision;/*!< Transmit excess collision*/
    uint32_t statsTxUnderFlow;      /*!< Transmit under flow*/
    uint32_t statsTxLarge;          /*!< Transmit large packet*/
    uint32_t statsTxSmall;          /*!< Transmit small packet*/
#endif
} enet_stats_t;

/*! @brief Defines the ENET MAC packet buffer structure.*/
typedef struct ENETMacPacketBuffer
{
    uint8_t *data;     /*!< Data buffer pointer*/
    uint16_t length;    /*!< Data length*/
    struct ENETMacPacketBuffer *next;  /*!< Next pointer*/
} enet_mac_packet_buffer_t;

#if ENET_RECEIVE_ALL_INTERRUPT
typedef uint32_t (* enet_netif_callback_t)(void *enetPtr, enet_mac_packet_buffer_t *packet);
#endif

/*! @brief Defines the receive buffer descriptor configure structure.*/
typedef struct ENETBuffConfig
{
    uint16_t rxBdNumber;    /*!< Receive buffer descriptor number*/
    uint16_t txBdNumber;    /*!< Transmit buffer descriptor number*/
    uint32_t rxBuffSizeAlign;    /*!< Aligned receive buffer size and must be larger than 256*/
    uint32_t txBuffSizeAlign;    /*!< Aligned transmit buffer size and must be larger than 256*/
    volatile enet_bd_struct_t  *rxBdPtrAlign; /*!< Aligned receive buffer descriptor pointer */
    uint8_t *rxBufferAlign;     /*!< Aligned receive data buffer pointer */
    volatile enet_bd_struct_t  *txBdPtrAlign; /*!< Aligned transmit buffer descriptor pointer*/
    uint8_t *txBufferAlign;     /*!< Aligned transmit buffer descriptor pointer*/
    uint8_t *extRxBuffQue;    /*!< Extended Rx data buffer queue to update the data buff
                                 in the receive buffer descriptor*/
    uint8_t extRxBuffNum;     /*!< extended data buffer number  */
#if FSL_FEATURE_ENET_SUPPORT_PTP
    uint32_t ptpTsRxBuffNum;      /*!< Receive 1588 timestamp buffer number*/
    uint32_t ptpTsTxBuffNum;     /*!< Transmit 1588 timestamp buffer number*/
    enet_mac_ptp_ts_data_t *ptpTsRxDataPtr; /*!< 1588 timestamp receive buffer pointer*/
    enet_mac_ptp_ts_data_t *ptpTsTxDataPtr; /*!< 1588 timestamp transmit buffer pointer*/
#endif
} enet_buff_config_t;


/*! @brief Defines the ENET device data structure for the ENET.*/
typedef struct ENETDevIf
{
    struct ENETDevIf *next; /*!< Next device structure address*/
    void *netIfPrivate;  /*!< For upper layer private structure*/  
    enet_multicast_group_t *multiGroupPtr; /*!< Multicast group chain*/
    uint32_t deviceNumber;    /*!< Device number*/
    uint8_t macAddr[kEnetMacAddrLen];        /*!< Mac address */
    uint8_t phyAddr;        /*!< PHY address connected to this device*/
    bool isInitialized;       /*!< Device initialized*/
    uint16_t maxFrameSize;  /*!< Mac maximum frame size*/
    bool isVlanTagEnabled;   /*!< ENET VLAN-TAG frames enabled*/
    bool isTxCrcEnable;         /*!< Transmit CRC enable in buffer descriptor*/
    bool isRxCrcFwdEnable;      /*!< Receive CRC forward*/
    enet_buff_descrip_context_t bdContext; /*!< Mac buffer descriptors context pointer*/
#if FSL_FEATURE_ENET_SUPPORT_PTP
    enet_private_ptp_buffer_t privatePtp;/*!< PTP private buffer*/
#endif
    enet_stats_t stats;                /*!< Packets statistic*/
#if ENET_RECEIVE_ALL_INTERRUPT
    enet_netif_callback_t  enetNetifcall;  /*!< Receive callback function to the upper layer*/
#else
    event_t enetReceiveSync;     /*!< Receive sync signal*/
#endif
    //mutex_t enetContextSync;     /*!< Sync signal*/
} enet_dev_if_t;

/*! @brief Defines the ENET user configuration structure. */
typedef struct ENETUserConfig
{
    const enet_mac_config_t* macCfgPtr;   /*!< MAC configuration structure */
    const enet_buff_config_t* buffCfgPtr;   /*!< ENET buffer configuration structure */
} enet_user_config_t;


/*******************************************************************************
 * API 
 ******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif

/*! 
  * @name ENET Driver
  * @{
  */


/*!
 * @brief Updates the receive buffer descriptor.
 *
 * This function updates the used receive buffer descriptor ring to
 * ensure that the used BDS is correctly used. It  cleans 
 * the status region and sets the control region of the used receive buffer 
 * descriptor. If the isBufferUpdate flag is set, the data buffer in the
 * buffer descriptor is updated.
 *
 * @param enetIfPtr The ENET context structure.
 * @param isBuffUpdate The data buffer update flag.
 * @return The execution status.
 */
enet_status_t ENET_DRV_UpdateRxBuffDescrip(enet_dev_if_t * enetIfPtr, bool isBuffUpdate);

/*!
 * @brief ENET transmit buffer descriptor cleanup.
 *
 * First, store the transmit frame error statistic and PTP timestamp of the transmitted packets. 
 * Second, clean up the used transmit buffer descriptors.
 * If the PTP 1588 feature is open, this interface  captures the 1588 timestamp. 
 * It is called by the transmit interrupt handler.
 *
 * @param enetIfPtr The ENET context structure.
 * @return The execution status.
 */
enet_status_t ENET_DRV_CleanupTxBuffDescrip(enet_dev_if_t * enetIfPtr);

/*!
 * @brief Increases the receive buffer descriptor to the next one.
 *
 * @param enetIfPtr The ENET context structure.
 * @param curBd The current buffer descriptor pointer.
 * @return the increased received buffer descriptor.
 */
volatile enet_bd_struct_t * ENET_DRV_IncrRxBuffDescripIndex(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd);

/*!
 * @brief Increases the transmit buffer descriptor to the next one.
 *
 * @param enetIfPtr The ENET context structure.
 * @param curBd The current buffer descriptor pointer.
 * @return the increased transmit buffer descriptor.
 */
volatile enet_bd_struct_t * ENET_DRV_IncrTxBuffDescripIndex(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd);

/*!
 * @brief Processes the ENET transmit frame statistics.
 *
 * This interface gets the error statistics of the transmit frame.
 * Because the error information is in the last BD of a frame, this interface
 * should be called when processing the last BD of a frame.
 *
 * @param enetIfPtr The ENET context structure.
 * @param curBd The current buffer descriptor.
 */
void ENET_DRV_TxErrorStats(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd);

/*!
 * @brief Processes the ENET receive frame error statistics.
 *
 * This interface gets the error statistics of the received frame.
 * Because the error information is in the last BD of a frame, this interface
 * should be called when processing the last BD of a frame.
 *
 * @param enetIfPtr The ENET context structure.
 * @param curBd The current buffer descriptor.
 * @return The frame error status.
 *         - True if the frame has an error. 
 *         - False if the frame does not have an error.
 */
bool ENET_DRV_RxErrorStats(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd);

/*!
 * @brief Receives ENET packets.
 *
 * @param enetIfPtr The ENET context structure.
 * @return The execution status.
 */
enet_status_t ENET_DRV_ReceiveData(enet_dev_if_t * enetIfPtr);


/*!
 * @brief ENET buffer enqueue.
 *
 * @param queue The buffer queue address.
 * @param buffer The buffer will add to the buffer queue.
 */
void enet_mac_enqueue_buffer( void **queue, void *buffer);

/*!
 * @brief ENET buffer dequeue.
 *
 * @param queue The buffer queue address.
 * @return The buffer will be dequeued from the buffer queue.
 */
void *enet_mac_dequeue_buffer( void **queue);

/* @} */

#if defined(__cplusplus)
}
#endif

/*! @}*/

#endif /* __FSL_ENET_DRIVER_H__ */
/*******************************************************************************
 * EOF
 ******************************************************************************/


