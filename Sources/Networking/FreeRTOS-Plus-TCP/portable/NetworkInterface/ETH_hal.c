/*
 * Copyright (c) 2013 - 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <string.h> 
#include "Application.h"

#include "ETH_hal.h"

/*******************************************************************************
 * Variables
 ******************************************************************************/
 
/*******************************************************************************
 * Code
 ******************************************************************************/




/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_ClrRxBdAfterHandled
 * Description: Update ENET receive buffer descriptors. The data is the 
 * buffer address and this address must always be evenly divisible by 16.
 *END*********************************************************************/
void ENET_HAL_ClrRxBdAfterHandled(volatile enet_bd_struct_t *rxBds, uint8_t *data, bool isbufferUpdate)
{
    assert(rxBds);

    if (isbufferUpdate)
    {
        assert(data);
        rxBds->buffer = (uint8_t *)BD_LONGSWAP((uint32_t)data);
    }
    rxBds->control &= kEnetRxBdWrap;  /* Clear status*/
    rxBds->control |= kEnetRxBdEmpty;   /* Set rx bd empty*/
#ifdef FSL_ENET_USE_ENHANCED_DESCRIPTORS
    rxBds->controlExtend1 |= kEnetRxBdIntrrupt;/* Enable interrupt*/
#endif
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_SetRMIIMode
 * Description: Configure (R)MII mode.
 *END*********************************************************************/
void ENET_HAL_SetRMIIMode(ENET_MemMapPtr base, enet_config_rmii_t *rmiiCfgPtr)
{
    uint32_t rcrReg;
    assert(rmiiCfgPtr);
    rcrReg = ENET_RD_RCR(base);

    rcrReg &= (~ENET_RCR_MII_MODE_MASK);                /* Set mii mode */
    rcrReg |= ENET_RCR_MII_MODE(1);
    rcrReg &= (~ENET_RCR_RMII_MODE_MASK);
    rcrReg |= ENET_RCR_RMII_MODE(rmiiCfgPtr->mode);
    rcrReg &= (~ENET_RCR_RMII_10T_MASK);
    rcrReg |= ENET_RCR_RMII_10T(rmiiCfgPtr->speed);     /* Set speed mode   */
    ENET_BWR_TCR_FDEN(base,rmiiCfgPtr->duplex);         /* Set duplex mode*/
    if ((!rmiiCfgPtr->duplex) && (rmiiCfgPtr->isRxOnTxDisabled))
    {
        rcrReg &= (~ENET_RCR_DRT_MASK);                 /* Disable receive on transmit*/
        rcrReg |= ENET_RCR_DRT(1);
    }

    if (rmiiCfgPtr->mode == kEnetCfgMii)                /* Set internal loop only for mii mode*/
    {
        rcrReg &= (~ENET_RCR_LOOP_MASK);
        rcrReg |= ENET_RCR_LOOP(rmiiCfgPtr->isLoopEnabled);
    }
    else
    {
        rcrReg &= (~ENET_RCR_LOOP_MASK);               /* Clear internal loop for rmii mode*/
        rcrReg |= ENET_RCR_LOOP(0);
    }
    ENET_WR_RCR(base, rcrReg);
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_SetSMIWrite
 * Description: Set SMI(serial Management interface) command.
 *END*********************************************************************/
void ENET_HAL_SetSMIWrite(ENET_MemMapPtr base, uint32_t phyAddr, uint32_t phyReg, enet_mii_write_t operation, uint32_t data)
{
    uint32_t mmfrValue = 0 ;

    mmfrValue = ENET_MMFR_ST(1)| ENET_MMFR_OP(operation)| ENET_MMFR_PA(phyAddr) | ENET_MMFR_RA(phyReg)| ENET_MMFR_TA(2) | (data&0xFFFF); /* mii command*/
    ENET_WR_MMFR(base,mmfrValue);
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_SetSMIRead
 * Description: Set SMI(serial Management interface) command.
 *END*********************************************************************/
void ENET_HAL_SetSMIRead(ENET_MemMapPtr base, uint32_t phyAddr, uint32_t phyReg, enet_mii_read_t operation)
{
    uint32_t mmfrValue = 0 ;

    mmfrValue = ENET_MMFR_ST(1)| ENET_MMFR_OP(operation)| ENET_MMFR_PA(phyAddr) | ENET_MMFR_RA(phyReg)| ENET_MMFR_TA(2); /* mii command*/
    ENET_WR_MMFR(base,mmfrValue);
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_GetBufDescripAttr
 * Description: Get the buffer descriptor attribute.
 *
 *END*********************************************************************/
void ENET_HAL_GetBufDescripAttr(volatile enet_bd_struct_t *curBd, const uint64_t mask, enet_bd_attr_t* resultAttr)
{
    uint16_t length;
#ifdef FSL_ENET_USE_ENHANCED_DESCRIPTORS
    uint32_t timestamp; 
#endif
    assert(curBd);
    assert(mask);
    assert(resultAttr);
    memset((void*)resultAttr, 0, sizeof(enet_bd_attr_t));
    if(mask & ENET_BD_CTL_MASK)
    {
        resultAttr->bdCtl         = curBd->control;
    }
#ifdef FSL_ENET_USE_ENHANCED_DESCRIPTORS
    if(mask & ENET_RX_BD_EXT_CTL_MASK)
    {
        resultAttr->rxBdExtCtl    = curBd->controlExtend0;
    }
    if(mask & ENET_RX_BD_EXT_CTL1_MASK)
    {
        resultAttr->rxBdExtCtl1   = curBd->controlExtend1;
    }
    if(mask & ENET_RX_BD_EXT_CTL2_MASK) 
    {
        resultAttr->rxBdExtCtl2   = curBd->controlExtend2;
    }
    if(mask & ENET_TX_BD_TIMESTAMP_FLAG_MASK)
    {      
        if(curBd->controlExtend1 & kEnetTxBdTimeStamp)
        {
            resultAttr->flags |= ENET_TX_BD_TIMESTAMP_FLAG;
        }
    }
#endif
    if(mask & ENET_BD_LEN_MASK)
    {
        length = curBd->length;
        resultAttr->bdLen = BD_SHORTSWAP(length);
    }
#ifdef FSL_ENET_USE_ENHANCED_DESCRIPTORS
    if(mask & ENET_BD_TIMESTAMP_MASK)
    {
        timestamp = curBd->timestamp;
        resultAttr->bdTimestamp = BD_LONGSWAP(timestamp);
    }
#endif
    if(mask & ENET_RX_BD_WRAP_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdWrap)
        {
            resultAttr->flags |= ENET_RX_BD_WRAP_FLAG;
        }
    }
    if(mask & ENET_RX_BD_EMPTY_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdEmpty)
        {
            resultAttr->flags |= ENET_RX_BD_EMPTY_FLAG;
        }
    }
    if(mask & ENET_RX_BD_TRUNC_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdTrunc)
        {
            resultAttr->flags |= ENET_RX_BD_TRUNC_FLAG;
        }
    }
    if(mask & ENET_RX_BD_LAST_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdLast)
        {
            resultAttr->flags |= ENET_RX_BD_LAST_FLAG;
        }
    }
    if(mask & ENET_TX_BD_READY_FLAG_MASK)
    {
        if(curBd->control & kEnetTxBdReady)
        {
            resultAttr->flags |= ENET_TX_BD_READY_FLAG;
        }
    }
    if(mask & ENET_TX_BD_LAST_FLAG_MASK)
    {
        if(curBd->control & kEnetTxBdLast)
        {
            resultAttr->flags |= ENET_TX_BD_LAST_FLAG;
        }
    }
    if(mask & ENET_TX_BD_WRAP_FLAG_MASK)
    {
        if(curBd->control & kEnetTxBdWrap)
        {
            resultAttr->flags |= ENET_TX_BD_WRAP_FLAG;
        }
    }
    if(mask & ENET_RX_BD_OVERRUN_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdOverRun)
        {
            resultAttr->flags |= ENET_RX_BD_OVERRUN_FLAG;
        }
    }
    if(mask & ENET_RX_BD_LEN_VIOLAT_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdLengthViolation)
        {
            resultAttr->flags |= ENET_RX_BD_LEN_VIOLAT_FLAG;
        }
    }
    if(mask & ENET_RX_BD_NO_OCTET_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdNoOctet)
        {
            resultAttr->flags |= ENET_RX_BD_NO_OCTET_FLAG;
        }
    }
    if(mask & ENET_RX_BD_CRC_ERR_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdCrc)
        {
            resultAttr->flags |= ENET_RX_BD_CRC_ERR_FLAG;
        }
    }
    if(mask & ENET_RX_BD_COLLISION_FLAG_MASK)
    {
        if(curBd->control & kEnetRxBdCollision)
        {
            resultAttr->flags |= ENET_RX_BD_COLLISION_FLAG;
        }
    }
#ifdef FSL_ENET_USE_ENHANCED_DESCRIPTORS
    /* Get extended control regions of the transmit buffer descriptor*/
    if(mask & ENET_TX_BD_TX_ERR_FLAG_MASK)
    {
        if(curBd->controlExtend0 & kEnetTxBdTxErr)
        {
            resultAttr->flags |= ENET_TX_BD_TX_ERR_FLAG;
        }
    }
    if(mask & ENET_TX_BD_EXC_COL_FLAG_MASK)
    {
        if(curBd->controlExtend0 & kEnetTxBdExcessCollisionErr)
        {
            resultAttr->flags |= ENET_TX_BD_EXC_COL_ERR_FLAG;
        }
    }
    if(mask & ENET_TX_BD_LATE_COL_FLAG_MASK)
    {
        if(curBd->controlExtend0 & kEnetTxBdLatecollisionErr)
        {
            resultAttr->flags |= ENET_TX_BD_LATE_COL_ERR_FLAG;
        }
    }
    if(mask & ENET_TX_BD_UNDERFLOW_FLAG_MASK)
    {
        if(curBd->controlExtend0 & kEnetTxBdTxUnderFlowErr)
        {
            resultAttr->flags |= ENET_TX_BD_UNDERFLOW_ERR_FLAG;
        }
    }
    if(mask & ENET_TX_BD_OVERFLOW_FLAG_MASK)
    {
        if(curBd->controlExtend0 & kEnetTxBdOverFlowErr)
        {
            resultAttr->flags |= ENET_TX_BD_OVERFLOW_FLAG;
        }
    }
#endif
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_HAL_GetBuffDescripData
 * Description: Get the buffer address of buffer descriptors.
 *END*********************************************************************/
uint8_t* ENET_HAL_GetBuffDescripData(volatile enet_bd_struct_t *curBd)
{
    uint32_t buffer;
    assert(curBd);

    buffer = (uint32_t)(curBd->buffer);
    return  (uint8_t *)BD_LONGSWAP(buffer);
}

/*******************************************************************************
 * EOF
 ******************************************************************************/

