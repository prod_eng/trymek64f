/*
* Copyright (c) 2013 - 2015, Freescale Semiconductor, Inc.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* o Redistributions of source code must retain the above copyright notice, this list
*   of conditions and the following disclaimer.
*
* o Redistributions in binary form must reproduce the above copyright notice, this
*   list of conditions and the following disclaimer in the documentation and/or
*   other materials provided with the distribution.
*
* o Neither the name of Freescale Semiconductor, Inc. nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
* ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <assert.h>
#include <string.h>
#include "Application.h"
#include "MK64F12.h"
#include "ETH_driver.h"
#include "ETH_hal.h"


/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_UpdateRxBuffDescrip
 * Return Value: The execution status.
 * Description: ENET receive buffer descriptor update.
 * This interface provides the receive buffer descriptor update and increase 
 * the current buffer descriptor pointer to the next one.
 *END*********************************************************************/
enet_status_t ENET_DRV_UpdateRxBuffDescrip(enet_dev_if_t * enetIfPtr, bool isBuffUpdate)
{
    ENET_MemMapPtr base;
    uint8_t *bufferTemp = NULL;

    base = g_enetBase[enetIfPtr->deviceNumber];

    while((enetIfPtr->bdContext.rxBdDirtyPtr != enetIfPtr->bdContext.rxBdCurPtr) ||
        (enetIfPtr->bdContext.isRxBdFull))
    {
        if(isBuffUpdate)
        {
            /* get the data buffer for update*/
            bufferTemp = (unsigned char*)enet_mac_dequeue_buffer((void **)&enetIfPtr->bdContext.extRxBuffQue); 
            if(!bufferTemp)
            {
                return kStatus_ENET_NoRxBufferLeft;
            }       
        }

        ENET_HAL_ClrRxBdAfterHandled(enetIfPtr->bdContext.rxBdDirtyPtr, bufferTemp, isBuffUpdate);

        /* Increase the buffer descriptor to the next one*/
        enetIfPtr->bdContext.rxBdDirtyPtr = 
            ENET_DRV_IncrRxBuffDescripIndex(enetIfPtr, enetIfPtr->bdContext.rxBdDirtyPtr);
        enetIfPtr->bdContext.isRxBdFull = FALSE;

        /* Active the receive buffer descriptor*/
        ENET_HAL_SetRxBdActive(base);
    }

    return kStatus_ENET_Success;
}
/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_CleanupTxBuffDescrip
 * Return Value: The execution status.
 * Description: First, store transmit frame error statistic and ptp timestamp 
 * of transmitted packets. Second, clean up the used transmit buffer descriptors.
 * If the ptp 1588 feature is open, this interface will do capture 1588 timestamp. 
 * It is called by transmit interrupt handler.
 *END*********************************************************************/
enet_status_t ENET_DRV_CleanupTxBuffDescrip(enet_dev_if_t * enetIfPtr)
{
    volatile enet_bd_struct_t *curBd;
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    mask |= (ENET_TX_BD_READY_FLAG_MASK | ENET_TX_BD_LAST_FLAG_MASK | ENET_TX_BD_TIMESTAMP_FLAG_MASK);
    
    while ((enetIfPtr->bdContext.txBdDirtyPtr != enetIfPtr->bdContext.txBdCurPtr)
           || (enetIfPtr->bdContext.isTxBdFull))
    {
        curBd = enetIfPtr->bdContext.txBdDirtyPtr;
        /* Get the control status data, If the bd has not been processed break out*/     
        ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
        if(bdAttr.flags & ENET_TX_BD_READY_FLAG)
        {
            break;
        }
#if FSL_FEATURE_ENET_SUPPORT_PTP
        if(enetIfPtr->privatePtp.firstflag)
        {
            enetIfPtr->privatePtp.firstBdPtr = curBd;
            enetIfPtr->privatePtp.firstflag = FALSE;
        }
#endif
        /* If the transmit buffer descriptor is ready, store packet statistic*/
        if(bdAttr.flags & ENET_TX_BD_LAST_FLAG)
        {
#if ENET_ENABLE_DETAIL_STATS
             ENET_DRV_TxErrorStats(enetIfPtr,curBd);
#endif
#if FSL_FEATURE_ENET_SUPPORT_PTP
            /* Do ptp timestamp store*/
            if (bdAttr.flags & ENET_TX_BD_TIMESTAMP_FLAG)
            {
                ENET_DRV_GetTxTs(&enetIfPtr->privatePtp, enetIfPtr->privatePtp.firstBdPtr, curBd);
                enetIfPtr->privatePtp.firstflag = true;
            }
#endif
        }

        /* Clear the buffer descriptor buffer address*/
        ENET_HAL_ClrTxBdAfterSend(curBd);

        /* Update the buffer address*/
        enetIfPtr->bdContext.txBdDirtyPtr = 
           ENET_DRV_IncrTxBuffDescripIndex(enetIfPtr, enetIfPtr->bdContext.txBdDirtyPtr);

        /* Clear the buffer full flag*/
        enetIfPtr->bdContext.isTxBdFull = FALSE;
    }
    
    return kStatus_ENET_Success;
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_IncrRxBuffDescripIndex
 * Return Value: The new rx buffer descriptor which number is increased one from old buffer descriptor.
 * Description: Increases the receive buffer descriptor to the next one.
 *END*********************************************************************/
volatile enet_bd_struct_t * ENET_DRV_IncrRxBuffDescripIndex(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd)
{
    assert(enetIfPtr);
    assert(curBd);
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    mask |= ENET_RX_BD_WRAP_FLAG_MASK;
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
    /* Increase the buffer descriptor, if it is the last one, increase to first one of the ring buffer*/
    if (bdAttr.flags & ENET_RX_BD_WRAP_FLAG)
    {
        curBd = enetIfPtr->bdContext.rxBdBasePtr;
    }
    else
    {
        curBd ++;
    }
    return curBd;
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_IncrTxBuffDescripIndex
 * Return Value: The new tx buffer descriptor which number is increased one from old buffer descriptor..
 * Description: Increases the transmit buffer descriptor to the next one.
 *END*********************************************************************/
volatile enet_bd_struct_t * ENET_DRV_IncrTxBuffDescripIndex(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd)
{
    /* Increase the buffer descriptor*/
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    mask |= ENET_TX_BD_WRAP_FLAG_MASK;
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
    if (bdAttr.flags & ENET_TX_BD_WRAP_FLAG)
    {
        curBd = enetIfPtr->bdContext.txBdBasePtr;
    }
    else
    {
        curBd ++;
    }
    return curBd;
}
/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_TxErrorStats
 * Return Value: None.
 * Description: ENET frame receive stats process.
 * This interface is used to process packet error statistic
 * in the last buffer descriptor of each frame.
 *END*********************************************************************/
void ENET_DRV_TxErrorStats(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd)
{
#if ENET_ENABLE_DETAIL_STATS
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    mask |= (ENET_TX_BD_TX_ERR_FLAG_MASK | ENET_TX_BD_EXC_COL_FLAG_MASK | \
      ENET_TX_BD_LATE_COL_FLAG_MASK | ENET_TX_BD_UNDERFLOW_FLAG_MASK | ENET_TX_BD_OVERFLOW_FLAG_MASK);
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);

    if (bdAttr.flags & ENET_TX_BD_TX_ERR_FLAG)
    {
        /* Transmit error*/
        enetIfPtr->stats.statsTxError++;

        if (bdAttr.flags & ENET_TX_BD_EXC_COL_ERR_FLAG)
        {
            /* Transmit excess collision*/
            enetIfPtr->stats.statsTxExcessCollision++;
        }
        else if (bdAttr.flags & ENET_TX_BD_LATE_COL_ERR_FLAG)
        {
            /* Transmit late collision*/
            enetIfPtr->stats.statsTxLateCollision++;
        }
        else if (bdAttr.flags & ENET_TX_BD_UNDERFLOW_ERR_FLAG)
        {
            /* Transmit underflow*/
            enetIfPtr->stats.statsTxUnderFlow++;
        }
        else if (bdAttr.flags & ENET_TX_BD_OVERFLOW_FLAG)
        {
            /* Transmit overflow*/
            enetIfPtr->stats.statsTxOverFlow++;
        }
    }
#endif
    enetIfPtr->stats.statsTxTotal++;
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_RxErrorStats
 * Return Value: true if the frame is error else false.
 * Description: ENET frame receive stats process.
 * This interface is used to process packet statistic in the last buffer
 * descriptor of each frame.
 *END*********************************************************************/
bool ENET_DRV_RxErrorStats(enet_dev_if_t * enetIfPtr, volatile enet_bd_struct_t *curBd)
{
    assert(enetIfPtr);
    assert(curBd);
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    /* The last bd in the frame check the stauts of the received frame*/
    mask |= (ENET_RX_BD_OVERRUN_FLAG_MASK | ENET_RX_BD_LEN_VIOLAT_FLAG_MASK | \
      ENET_RX_BD_NO_OCTET_FLAG_MASK | ENET_RX_BD_CRC_ERR_FLAG_MASK | ENET_RX_BD_COLLISION_FLAG_MASK);
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);    
   
    if ((bdAttr.flags & ENET_RX_BD_OVERRUN_FLAG) || (bdAttr.flags & ENET_RX_BD_LEN_VIOLAT_FLAG) \
      || (bdAttr.flags & ENET_RX_BD_NO_OCTET_FLAG) || (bdAttr.flags & ENET_RX_BD_CRC_ERR_FLAG)\
        || (bdAttr.flags & ENET_RX_BD_COLLISION_FLAG))
    {
#if ENET_ENABLE_DETAIL_STATS         
        /* Discard error packets*/
        enetIfPtr->stats.statsRxError++;
        enetIfPtr->stats.statsRxDiscard++;

        /* Receive error*/
        if (bdAttr.flags & ENET_RX_BD_OVERRUN_FLAG)
        {
            /* Receive over run*/
            enetIfPtr->stats.statsRxOverRun++;
        }
        else if (bdAttr.flags & ENET_RX_BD_LEN_VIOLAT_FLAG)
        {
            /* Receive length greater than max frame*/
            enetIfPtr->stats.statsRxLengthGreater++;
        }
        else if (bdAttr.flags & ENET_RX_BD_NO_OCTET_FLAG)
        {
            /* Receive non-octet aligned frame*/
            enetIfPtr->stats.statsRxAlign++;
        }
        else if (bdAttr.flags & ENET_RX_BD_CRC_ERR_FLAG)
        {
            /* Receive crc error*/
            enetIfPtr->stats.statsRxFcs++;
        }
        else if (bdAttr.flags & ENET_RX_BD_COLLISION_FLAG)
        { 
            /* late collision frame discard*/
            enetIfPtr->stats.statsRxCollision++;
        }
#endif
        return TRUE;
    }
    else
    {
        /* Add the right packets*/            
        enetIfPtr->stats.statsRxTotal++;
        return FALSE;
    }
}

/*FUNCTION****************************************************************
 *
 * Function Name: ENET_DRV_ReceiveData
 * Return Value: The execution status.
 * Description: ENET frame receive function.
 * This interface receive the frame from ENET deviece and returns the address 
 * of the received data. This is used to do receive data in receive interrupt
 * handler. This is the pure interrupt mode
 *END*********************************************************************/
enet_status_t ENET_DRV_ReceiveData(enet_dev_if_t * enetIfPtr)
{
    volatile enet_bd_struct_t *curBd;
    uint16_t bdNumTotal = 0, lenTotal = 0;
    uint64_t mask = 0;
    enet_bd_attr_t bdAttr;
    enet_mac_packet_buffer_t packetBuffer[kEnetMaxFrameBdNumbers] = {{0}};
    /* Check input parameters*/
    if ((!enetIfPtr) || (!enetIfPtr->bdContext.rxBdCurPtr))
    {
        return kStatus_ENET_InvalidInput;
    }
   
    /* Check the current buffer descriptor address*/
    curBd = enetIfPtr->bdContext.rxBdCurPtr;
    /* Return if the current buffer descriptor is empty*/	
    mask |= (ENET_RX_BD_EMPTY_FLAG_MASK | ENET_RX_BD_TRUNC_FLAG_MASK | ENET_RX_BD_LAST_FLAG_MASK | ENET_BD_LEN_MASK);
    ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
    while(!(bdAttr.flags & ENET_RX_BD_EMPTY_FLAG))
    {
        /* Check if receive buffer is full*/
        if(enetIfPtr->bdContext.isRxBdFull)
        {
#if ENET_ENABLE_DETAIL_STATS 
            enetIfPtr->stats.statsRxDiscard++;
#endif
            ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
            return kStatus_ENET_RxBdFull;
        }

        /* Increase current buffer descriptor to the next one*/
        enetIfPtr->bdContext.rxBdCurPtr = 
            ENET_DRV_IncrRxBuffDescripIndex(enetIfPtr, enetIfPtr->bdContext.rxBdCurPtr);
        /* Check if the buffer is full*/
        if (enetIfPtr->bdContext.rxBdCurPtr == enetIfPtr->bdContext.rxBdDirtyPtr)
        {
            enetIfPtr->bdContext.isRxBdFull = TRUE;
        }

         /* Discard packets with truncate error*/
        if (bdAttr.flags & ENET_RX_BD_TRUNC_FLAG)
        {
#if ENET_ENABLE_DETAIL_STATS 
            enetIfPtr->stats.statsRxTruncate++;
            enetIfPtr->stats.statsRxDiscard++;
#endif
            ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
            return kStatus_ENET_RxbdTrunc;
        }
        
       if (bdAttr.flags & ENET_RX_BD_LAST_FLAG) 
       {
           /* The last bd in the frame check the stauts of the received frame*/
           if (ENET_DRV_RxErrorStats(enetIfPtr, curBd))
           {
               ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
               return kStatus_ENET_RxbdError;
           }
           else
           {
                packetBuffer[bdNumTotal].data = ENET_HAL_GetBuffDescripData(curBd);
                packetBuffer[bdNumTotal].length = bdAttr.bdLen - lenTotal;
                /* Crc length check */
                if(enetIfPtr->isRxCrcFwdEnable)
                {
                    packetBuffer[bdNumTotal].length -= kEnetFrameFcsLen;
                }
                packetBuffer[bdNumTotal].next = NULL;
#if FSL_FEATURE_ENET_SUPPORT_PTP
                ENET_DRV_GetRxTs(&enetIfPtr->privatePtp, 
                    packetBuffer[0].data, curBd);
#endif
                if(enetIfPtr->enetNetifcall)
                {
                    enetIfPtr->enetNetifcall(enetIfPtr, &packetBuffer[0]);
                }

               /* Update receive buffer descriptor*/
               ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
               bdNumTotal = 0;
           }
        }
        else
        {
            packetBuffer[bdNumTotal].data = ENET_HAL_GetBuffDescripData(curBd);
            packetBuffer[bdNumTotal].length = enetIfPtr->bdContext.rxBuffSizeAlign;
            lenTotal += packetBuffer[bdNumTotal].length;
            packetBuffer[bdNumTotal].next = &packetBuffer[bdNumTotal + 1];
            bdNumTotal ++;
            /* Check a frame with total bd numbers */
            if (bdNumTotal == kEnetMaxFrameBdNumbers)
            {
#if ENET_ENABLE_DETAIL_STATS 
                enetIfPtr->stats.statsRxDiscard++;
#endif
                ENET_DRV_UpdateRxBuffDescrip(enetIfPtr, FALSE);
                return kStatus_ENET_SmallRxBuffSize;
            }
        }

        /* Check the current buffer descriptor address*/
        curBd = enetIfPtr->bdContext.rxBdCurPtr;
        ENET_HAL_GetBufDescripAttr(curBd, mask, &bdAttr);
    }

    return kStatus_ENET_Success;
}



/*FUNCTION****************************************************************
 *
 * Function Name: enet_mac_enqueue_buffer
 * Return Value: 
 * Description: ENET mac enqueue buffers.
 * This function is used to enqueue buffers to buffer queue.
 *END*********************************************************************/
void enet_mac_enqueue_buffer( void **queue, void *buffer)
{
    *((void **)buffer) = *queue;
    *queue = buffer;
}

/*FUNCTION****************************************************************
 *
 * Function Name: enet_mac_dequeue_buffer
 * Return Value: The dequeued buffer pointer
 * Description: ENET mac dequeue buffers.
 * This function is used to dequeue a buffer from buffer queue.
 *END*********************************************************************/
void *enet_mac_dequeue_buffer( void **queue)
{
    void *buffer = *queue;

    if (buffer) 
    {
        *queue = *((void **)buffer);
    }
    
    return buffer;
}


/*******************************************************************************
 * EOF
 ******************************************************************************/

