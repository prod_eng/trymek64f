/** Portable structure packing pragma
 *  Returns to default packing of compiler
 */

 /* FreeRTOS structures don't have ';' for cases where an __attribute is appended */
;  // so we include one here to close the structure definition
#pragma pack()
