/*
    FreeRTOS V8.2.2 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/


/*****************************************************************************
 *
 * See the following URL for configuration information.
 * http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/TCP_IP_Configuration.html
 *
 *****************************************************************************/
 
#ifndef FREERTOS_IP_CONFIG_H
#define FREERTOS_IP_CONFIG_H

#define ipconfigIP_TASK_STACK_SIZE_WORDS        (configMINIMAL_STACK_SIZE * 6)
#define ipconfigIP_TASK_PRIORITY                (configMAX_PRIORITIES - 2)

/* Set to 1 to print out debug messages.  If ipconfigHAS_DEBUG_PRINTF is set to
1 then FreeRTOS_debug_printf should be defined to the function used to print
out the debugging messages. */
#define ipconfigHAS_DEBUG_PRINTF                0

/* Set to 1 to print out non debugging messages, for example the output of the
FreeRTOS_netstat() command, and ping replies.  If ipconfigHAS_PRINTF is set to 1
then FreeRTOS_printf should be set to the function used to print out the
messages. */
#define ipconfigHAS_PRINTF                      0

/* Define the byte order of the target MCU (the MCU FreeRTOS+TCP is executing
on).  Valid options are pdFREERTOS_BIG_ENDIAN and pdFREERTOS_LITTLE_ENDIAN. */
#define ipconfigBYTE_ORDER pdFREERTOS_LITTLE_ENDIAN

/* ipconfigRAND32() is called by the IP stack to generate random numbers for
things such as a DHCP transaction number or initial sequence number.  Random
number generation is performed via this macro to allow applications to use their
own random number generation method.  For example, it might be possible to
generate a random number by sampling noise on an analogue input. */
extern UBaseType_t Random32(void);
#define ipconfigRAND32()                        Random32()

#define ipconfigSUPPORT_OUTGOING_PINGS              0
#define ipconfigETHERNET_DRIVER_FILTERS_FRAME_TYPES 1

/* Advanced only: in order to access 32-bit fields in the IP packets with
32-bit memory instructions, all packets will be stored 32-bit-aligned, plus 16-bits.
This has to do with the contents of the IP-packets: all 32-bit fields are
32-bit-aligned, plus 16-bit(!) */
#define ipconfigPACKET_FILLER_SIZE              2

/* If ipconfigUSE_DHCP is 1 then FreeRTOS+TCP will attempt to retrieve an IP
address, netmask, DNS server address and gateway address from a DHCP server.  If
ipconfigUSE_DHCP is 0 then FreeRTOS+TCP will use a static IP address.  The
stack will revert to using the static IP address even when ipconfigUSE_DHCP is
set to 1 if a valid configuration cannot be obtained from a DHCP server for any
reason.  The static configuration used is that passed into the stack by the
FreeRTOS_IPInit() function call. */
#define ipconfigUSE_DHCP                        1
#define ipconfigDHCP_USES_USER_HOOK				1

/* ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS defines the total number of network buffer that
are available to the IP stack.  The total number of network buffers is limited
to ensure the total amount of RAM that can be consumed by the IP stack is capped
to a pre-determinable value. */
// ARH - changed
#define ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS		2

// ARH - disabling sliding windows and reducing sizes to minimize memory impact. 
#define ipconfigUSE_TCP_WIN				( 0 )

#define ipconfigTCP_TX_BUFFER_LENGTH   ( 1 * ipconfigTCP_MSS )
#define ipconfigTCP_RX_BUFFER_LENGTH   ( 1 * ipconfigTCP_MSS )

#define ipconfigNETWORK_MTU     1526
#define ipconfigTCP_MSS         1460


#define ipconfigTCP_RX_BUF_LEN			( 2 * ipconfigTCP_MSS )
#define ipconfigTCP_TX_BUF_LEN			( 4 * ipconfigTCP_MSS )

extern void debug_IP_trace(uint32_t type);
#define iptraceFAILED_TO_OBTAIN_NETWORK_BUFFER()				debug_IP_trace(1)
#define iptraceFAILED_TO_OBTAIN_NETWORK_BUFFER_FROM_ISR()		debug_IP_trace(2)
#define iptraceFAILED_TO_CREATE_SOCKET()						debug_IP_trace(3)
#define iptraceETHERNET_RX_EVENT_LOST()							debug_IP_trace(4)
#define iptraceSTACK_TX_EVENT_LOST(x)							debug_IP_trace(5)
#define iptraceBIND_FAILED(x,y)									debug_IP_trace(6)

#define iptraceDHCP_REQUESTS_FAILED_USING_DEFAULT_IP_ADDRESS(x)	vEthernetSignalDHCPFailed()

extern void vAppWatchdogResetIPTaskCtr(void);
#define ipconfigWATCHDOG_TIMER()		vAppWatchdogResetIPTaskCtr()

// To make sure the IP task services its watchdog variable often enough we need
// to reduce its maximum sleep time.  We will set it to 250ms
#define ipconfigMAX_IP_TASK_SLEEP_TIME ( pdMS_TO_TICKS( 250UL ) )


#endif /* FREERTOS_IP_CONFIG_H */
