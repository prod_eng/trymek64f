/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppEthernetModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Mar 15, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APP_ETHERNET_MODULE_H_
#define SOURCES_APP_ETHERNET_MODULE_H_

#include "ETH_hal.h"

#define cIP_ADDRESS_LENGTH_BYTES    ( 4 )
#define cMAC_ADDRESS_LENGTH_BYTES   ( 6 )

// These macros are supplemental to the MK60D10.h file, and are here because that file gets auto-generated.
#define ENET_RCR_LOOP(x)                         (((uint32_t)(((uint32_t)(x))<<ENET_RCR_LOOP_SHIFT))&ENET_RCR_LOOP_MASK)
#define ENET_RCR_DRT(x)                          (((uint32_t)(((uint32_t)(x))<<ENET_RCR_DRT_SHIFT))&ENET_RCR_DRT_MASK)
#define ENET_RCR_MII_MODE(x)                     (((uint32_t)(((uint32_t)(x))<<ENET_RCR_MII_MODE_SHIFT))&ENET_RCR_MII_MODE_MASK)
#define ENET_RCR_RMII_MODE(x)                    (((uint32_t)(((uint32_t)(x))<<ENET_RCR_RMII_MODE_SHIFT))&ENET_RCR_RMII_MODE_MASK)
#define ENET_RCR_RMII_10T(x)                     (((uint32_t)(((uint32_t)(x))<<ENET_RCR_RMII_10T_SHIFT))&ENET_RCR_RMII_10T_MASK)


#define HTTP_PORT_NUM ( 80 )
#define SSH_PORT_NUM  ( 22 )

void vAppEthernetRxTaskInit( uint8 u8Priority, uint16 u16StackSize );
void vAppEthernetTxTaskInit( uint8 u8Priority, uint16 u16StackSize );
bool boEthernetRxWatchdogTaskCheck(void);
bool boEthernetTxWatchdogTaskCheck(void);
void vEthernetRxWatchdogTaskClear(void);
void vEthernetTxWatchdogTaskClear(void);


// TODO - Do we still need this?
//void Network_OnIDSCanMessageTx(const IDS_CAN_RX_MESSAGE *message);

BaseType_t xApplicationDHCPUserHook(    eDHCPCallbackQuestion_t eQuestion,
                                        uint32_t ulIPAddress,
                                        uint32_t ulNetMask );


const uint8* pEthernetGetAdapterMAC(void);
void vEthernetSignalDHCPFailed(void);
uint8 u8EthernetGetConnectionCount(void);
uint32 u32EthernetGetQueueErrCnt(void);

void vAppEthernetModuleTx_ISR( LDD_TUserData *pUserData );
void vAppEthernetModuleRx_ISR( LDD_TUserData *pUserData, uint16 u16FragCount, uint16 u16Length );


#endif // SOURCES_APP_ETHERNET_MODULE_H_
