/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppWebPages.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Jun 22, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_NETWORKING_APPWEBPAGES_H_
#define SOURCES_NETWORKING_APPWEBPAGES_H_


typedef enum eWebPageToBuildType
{
    eWEB_PAGE_TO_BUILD_VERSIONS = 0,
    //
    eWEB_PAGE_TO_BUILD_COUNT
}eWEB_PAGE_TO_BUILD_TYPE;


uint8 *pu8WebGeneratePage( eWEB_PAGE_TO_BUILD_TYPE ePageToBuild, uint32 *pNewLen );

#endif // SOURCES_NETWORKING_APPWEBPAGES_H_
