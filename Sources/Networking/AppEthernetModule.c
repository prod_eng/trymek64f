/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppEthernetModule
 *
 * @brief           TCP / UDP networking for IDS Production Testers and utility hardware
 *
 * @date            March 14, 2015
 *
 * @author
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"
#include "NetworkConfigDefaults.h"
#include "ETH0.h"


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */

#if TCP_MAX_CONNECTION_CNT > 4
#error TCP_MAX_CONNECTION_CNT must be 4 or less.
#endif

#define SOCK_INFO_QUEUE_LENGTH          TCP_MAX_CONNECTION_CNT
#define SOCK_INFO_QUEUE_ITEM_SIZE       sizeof( xSocketConnectionInfoType )
//#define CAN_MSG_QUEUE_LENGTH            ( 64 )
//#define CAN_MSG_QUEUE_ITEM_SIZE         sizeof( CAN_Msg_And_Socket_t )
#define RX_MSG_SOCK_INDEX               ( 0xFE )
#define IDS_CAN_TX_SOCK_INDEX           ( 0xFF )
static const uint32 cDONT_WAIT = 0;


// struct for passing connection info to threads
typedef struct
{
    Socket_t socket;
    uint8 index;
    uint8 active;
} xSocketConnectionInfoType;


// struct for storing CAN messages as well as the socket they came from
//typedef struct
//{
//    uint8 socketIndex;
//    CAN_RX_MESSAGE message;
//} CAN_Msg_And_Socket_t;


// struct for the arguments of the tcp worker threads
typedef struct
{
    Socket_t socket;
    uint8 socketIndex;
    QueueHandle_t responseQueue;
} TCP_Worker_Thread_Args_t;


// struct for the arguments of the tcp tx threads
typedef struct
{
    Socket_t socket;
    uint8 socketIndex;
    QueueHandle_t canMsgQueue;
} TCP_Tx_Thread_Args_t;



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
static volatile uint32 u32MyRxTaskWdgCheck        = FALSE;
static TaskHandle_t xMyRxTaskHandle               = NULL;

static volatile uint32 u32MyTxTaskWdgCheck        = FALSE;
static TaskHandle_t xMyTxTaskHandle               = NULL;

static QueueHandle_t Sock_Info_queue;
static QueueHandle_t CAN_Msg_queue[TCP_MAX_CONNECTION_CNT];

static uint8 gu8EthIPv4Address[4];  // Initialized at EthernetTaskInit()
static uint8 gu8EthIPv4Netmask[4];  // Initialized at EthernetTaskInit()
static uint8 gu8EthIPv4Gateway[4];  // Initialized at EthernetTaskInit()
static uint8 gu8EthIPv4DNSAddr[4];  // Initialized at EthernetTaskInit()
static uint8 gu8EthMACAddress[6];   // Initialized at EthernetTaskInit()

static uint8 TCPConnectionCnt = 0;
static uint8 socketInActive[TCP_MAX_CONNECTION_CNT];
static uint32 u32NetQueueErrCnt = 0;


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskTcpRx( void *pvParams ) __attribute__ ((noreturn));
static void vAppTaskTcpTx( void *pvParams ) __attribute__ ((noreturn));
static void vAppTaskTcpWorker( void *pvParams );
static void vAppProcessHTTPTask( void *pvParams );
static void vAppTaskUdpBcast( void *pvParams ) __attribute__ ((noreturn));

//static void vSendMsgToCANQueues( CAN_Msg_And_Socket_t *msg );
static void TCP_Process_Special_Msg( const uint8 * message );


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppEthernetRxTaskInit( uint8 u8Priority, uint16 u16StackSize )
{
    BaseType_t taskCreationStatus;
    uint8_t i = 0;

//    memcpy( &gu8EthIPv4Address, pFlashAskIPv4Address(), ipIP_ADDRESS_LENGTH_BYTES );
//    memcpy( &gu8EthIPv4Netmask, pFlashAskIPv4Netmask(), ipIP_ADDRESS_LENGTH_BYTES );
//    memcpy( &gu8EthIPv4Gateway, pFlashAskIPv4Gateway(), ipIP_ADDRESS_LENGTH_BYTES );
//    memcpy( &gu8EthIPv4DNSAddr, pFlashAskIPv4DNSAddr(), ipIP_ADDRESS_LENGTH_BYTES );
//    memcpy( &gu8EthMACAddress, pFlashAskMacAddress(), ipMAC_ADDRESS_LENGTH_BYTES );

    uint8_t dns_addr[ipIP_ADDRESS_LENGTH_BYTES] = HARDCODED_DNS_SERV;
    gu8EthIPv4DNSAddr[0] = dns_addr[0];
    gu8EthIPv4DNSAddr[1] = dns_addr[1];
    gu8EthIPv4DNSAddr[2] = dns_addr[2];
    gu8EthIPv4DNSAddr[3] = dns_addr[3];

    // Initialize the networking stack
    FreeRTOS_IPInit( gu8EthIPv4Address, gu8EthIPv4Netmask, gu8EthIPv4Gateway, gu8EthIPv4DNSAddr, gu8EthMACAddress );

    // Create the queue for passing connection information from the rx thread to the tx thread
    Sock_Info_queue = FRTOS1_xQueueCreate( SOCK_INFO_QUEUE_LENGTH, SOCK_INFO_QUEUE_ITEM_SIZE );

    if( Sock_Info_queue == NULL ) // If we couldn't create the queue...
    {
        CPU_SystemReset(); // ... then die
    }

    // Create the queues for storing CAN messages
//    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
//    {
//        CAN_Msg_queue[i] = FRTOS1_xQueueCreate( CAN_MSG_QUEUE_LENGTH, CAN_MSG_QUEUE_ITEM_SIZE );
//
//        if( CAN_Msg_queue[i] == NULL )
//        {
//            CPU_SystemReset();
//        }
//    }




    // Create the Rx task
    taskCreationStatus = FRTOS1_xTaskCreate( vAppTaskTcpRx,
                                              "TCPReceive",
                                              u16StackSize,
                              (void *)&u32MyRxTaskWdgCheck,
                                                u8Priority,
                                          &xMyRxTaskHandle );

    if( errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY == taskCreationStatus )
    {
        CPU_SystemReset();
    }

    return;
}



void vAppEthernetTxTaskInit( uint8 u8Priority, uint16 u16StackSize )
{
    portBASE_TYPE taskCreationStatus;
    // Create the UDP broadcast task
    taskCreationStatus = FRTOS1_xTaskCreate(    vAppTaskUdpBcast,
                                                      "UDPBCast",
                                                    u16StackSize,
                                    (void *)&u32MyTxTaskWdgCheck,
                                                      u8Priority,
                                                &xMyTxTaskHandle );

    if( errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY == taskCreationStatus )
    {
        CPU_SystemReset();
    }

    return;

}


bool boEthernetRxWatchdogTaskCheck(void)
{
    bool boReturn = FALSE;

    // Verify that our task is still running by testing its watchdog variable
    if( u32MyRxTaskWdgCheck )
    {
        // The watchdog has changed so everything is good.
        boReturn = TRUE;
    }

    return boReturn;
}




bool boEthernetTxWatchdogTaskCheck(void)
{
    bool boReturn = FALSE;

    // Verify that our task is still running by testing its watchdog variable
    if( u32MyTxTaskWdgCheck )
    {
        // The watchdog has changed so everything is good.
        boReturn = TRUE;
    }

    return boReturn;
}




void vEthernetRxWatchdogTaskClear(void)
{
    // Reset the check variable so we can tell if it has been set by the next time around.
    u32MyRxTaskWdgCheck = FALSE;
}




void vEthernetTxWatchdogTaskClear(void)
{
    // Reset the check variable so we can tell if it has been set by the next time around.
    u32MyTxTaskWdgCheck = FALSE;
}



//
//void Network_OnCANMessageRx( const CAN_RX_MESSAGE *message )
//{
//    CAN_Msg_And_Socket_t msgAndSock;
//
//    msgAndSock.message = *message;
//    // for RX messages, use an invalid socket index.  this way,
//    // we will never tell any socket that this is their echo
//    msgAndSock.socketIndex = RX_MSG_SOCK_INDEX;
//
//    vSendMsgToCANQueues( &msgAndSock );
//
//    // toggle the amber status light every time we receive a message
//    // TODO Got rid of this pin mapping in order to support separate crystal on uP
//    //GPIO_DRV_TogglePinOutput( LED1_Control );
//}


void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkUp )
{
    // Called by the IP stack when an IP address is assigned
    union uniLongToBytes
    {
        uint32 ulIPAddr;
        uint8  u8Octets[4];
    }ipAddressAssigned;
    uint8 strIPString[] = "xxx.yyy.zzz.aaa";

    if( 0 != *ipLOCAL_IP_ADDRESS_POINTER )
    {
        ipAddressAssigned.ulIPAddr = *ipLOCAL_IP_ADDRESS_POINTER; // Get the assigned IP address as a U32 and convert to bytes for display output.
        sprintf( (char*)strIPString, "%03d.%03d.%03d.%03d", ipAddressAssigned.u8Octets[0],  ipAddressAssigned.u8Octets[1], ipAddressAssigned.u8Octets[2], ipAddressAssigned.u8Octets[3] );
    }
    // Display the IP on our LCD
    //vLCDDisplayAtXY( 0, 1, (uint8 *)( &strIPString ) );

    return;
}


#ifdef INCLUDE_IDS_CORE_CAN
void Network_OnIDSCanMessageTx( const IDS_CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;
//TODO this can be re-done:
    msgAndSock.message.Length = message->Length;
    msgAndSock.message.Data[0] = message->Data[0];
    msgAndSock.message.Data[1] = message->Data[1];
    msgAndSock.message.Data[2] = message->Data[2];
    msgAndSock.message.Data[3] = message->Data[3];
    msgAndSock.message.Data[4] = message->Data[4];
    msgAndSock.message.Data[5] = message->Data[5];
    msgAndSock.message.Data[6] = message->Data[6];
    msgAndSock.message.Data[7] = message->Data[7];
    msgAndSock.message.Timestamp_ms = message->Timestamp_ms;
    msgAndSock.message.ID = IDS_CAN_PackID( &message->ID );

    // this is a message that was TX'd from the IDS-CAN layer, it is not
    // an echo to any source
    msgAndSock.socketIndex = IDS_CAN_TX_SOCK_INDEX;

    vSendMsgToCANQueues( &msgAndSock );
}
#endif // INCLUDE_IDS_CORE_CAN



BaseType_t xApplicationDHCPUserHook( eDHCPCallbackQuestion_t eQuestion,
            uint32_t ulIPAddress,
            uint32_t ulNetMask )
{
    BaseType_t xResult = (BaseType_t)eDHCPContinue;

    switch( eQuestion )
    {
        case eDHCPOffer : /* Driver is about to ask for a DHCP offer. */
        case eDHCPRequest : /* Driver is about to request DHCP an IP address. */
            if( TRUE ) // ( 1 != boFlashAskDHCPMode() )
            {
                /* Return eDHCPUseDefaults to abort the DHCP process. */
                xResult = (BaseType_t)eDHCPUseDefaults;
            }
            break;
            /* In all other instances, return eDHCPContinue. */
    }

    return xResult;
}




const uint8* pEthernetGetAdapterMAC(void)
{
    return ( &gu8EthMACAddress[0] );
}




void vEthernetSignalDHCPFailed( void )
{
    // The network stack has failed to obtain a DHCP address.  If we are configured to use DHCP,
    // restart the process by forcing the stack to initialize again.
    if(1) // ( boFlashAskDHCPMode() )
    {
        // we are configured to use DHCP.  Force the stack to try initializing again by
        // telling it the network driver went down
        FreeRTOS_NetworkDown();
    }
}




uint8 u8EthernetGetConnectionCount(void)
{
    return TCPConnectionCnt;
}




uint32 u32EthernetGetQueueErrCnt(void)
{
    return  u32NetQueueErrCnt;
}



void vAppEthernetModuleTx_ISR( LDD_TUserData *pUserData )
{
    // The buffer contents have been transmitted
    // Release the transmit buffer
    // TODO
    return;
}





/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (Module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskTcpRx( void *pvParams )
{
    volatile uint32 *pu32TaskIsRunning;
    // Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
    pu32TaskIsRunning = (volatile uint32 *)pvParams;

    uint32_t i = 0;
    Socket_t socket;
    Socket_t socketIn[TCP_MAX_CONNECTION_CNT];
    TaskHandle_t txTaskHandles[TCP_MAX_CONNECTION_CNT] = { NULL };
    struct freertos_sockaddr socketAddress;
    struct freertos_sockaddr socketAddressIn;
    socklen_t addrLen;
    TickType_t timeout;
    uint32_t result = 0;

#define WORKER_THREAD_QUEUE_LENGTH TCP_MAX_CONNECTION_CNT
#define WORKER_THREAD_QUEUE_ITEM_SIZE sizeof(xSocketConnectionInfoType)
    QueueHandle_t Worker_Thread_Queue;

    // initialize active socket flags
    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
    {
        socketInActive[i] = 0;
    }

    // Create the worker thread queue
    Worker_Thread_Queue = FRTOS1_xQueueCreate( WORKER_THREAD_QUEUE_LENGTH, WORKER_THREAD_QUEUE_ITEM_SIZE );

    if( NULL == Worker_Thread_Queue )
    {
        // We weren't able to create the queue!
        CPU_SystemReset();
    }

    // Create the TCP socket
    socket = FreeRTOS_socket( FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP );

    if( socket == FREERTOS_INVALID_SOCKET )
    {
        // We weren't able to create the socket!
        CPU_SystemReset();
    }

    timeout =  100;

    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_RCVTIMEO, &timeout, 0 );

    if( result != 0 )
    {
        // We weren't able to configure the Rx timing for the socket!
        CPU_SystemReset();
    }

    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_SNDTIMEO, &timeout, 0 );

    if( result != 0 )
    {
        // We weren't able to configure the Tx timing for the socket!
        CPU_SystemReset();
    }

    // Bind connection to TCP port
    socketAddress.sin_port = FreeRTOS_htons( HTTP_PORT_NUM );

    result = FreeRTOS_bind( socket, &socketAddress, sizeof ( socketAddress ) );

    if( result != 0 )
    {
        // We weren't able to bind the Rx socket!
        CPU_SystemReset();
    }

    // Tell connection to go into listening mode
    result = FreeRTOS_listen( socket, TCP_MAX_CONNECTION_CNT );

    if( result != 0 )
    {
        // We weren't able to configure the Rx socket!
        CPU_SystemReset();
    }


    // Configuration of the sockets is complete...

    for(;;) // Task thread stays in this loop
    {
        uint8 indexToUse = 0;
        uint8 socketFree = 0;
        xSocketConnectionInfoType workerResponse;

        // Kick the dog
        FRTOS1_taskENTER_CRITICAL();
        *pu32TaskIsRunning = TRUE;
        FRTOS1_taskEXIT_CRITICAL();


        // check for new socket connections from the worker threads
        if( pdPASS == FRTOS1_xQueueReceive( Worker_Thread_Queue, &workerResponse, cDONT_WAIT ) )
        {
            // the receive socket is now free
            socketInActive[workerResponse.index] = 0;

            // inform the watchdog module that both the worker and tx task are now inactive
            //TODO vAppWatchdogUpdateTxTaskStatus( workerResponse.index, FALSE );
            //TODO vAppWatchdogUpdateWorkerTaskStatus( workerResponse.index, FALSE );

            // kill the Tx task
            //TODO FRTOS1_vTaskDelete( txTaskHandles[workerResponse.index] );
        }

        // find a free socket
        indexToUse = 0;
        socketFree = 0;

        for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
        {
            if( socketInActive[i] == 0 )
            {
                indexToUse = i;
                socketFree = 1;
                break;
            }
        }

        if( socketFree != 0 )
        {
            // attempt to get a connection on this socket
            addrLen = sizeof ( socketAddressIn );
            socketIn[indexToUse] = FreeRTOS_accept( socket, &socketAddressIn, &addrLen );

            /* Process the new connection. */
            if( ( socketIn[indexToUse] != FREERTOS_INVALID_SOCKET ) && ( socketIn[indexToUse] != NULL ) )
            {
                uint32_t result = 0;

                socketInActive[indexToUse] = 1;

                // create the worker thread for this connection
                TCP_Worker_Thread_Args_t worker_args;
                worker_args.socket = socketIn[indexToUse];
                worker_args.socketIndex = indexToUse;
                worker_args.responseQueue = Worker_Thread_Queue;
#if 0 //RB - Not sure if this is needed
                result = FRTOS1_xTaskCreate( vAppTaskTcpWorker,
                                                     "TCPWorker",
                                        configMINIMAL_STACK_SIZE,
                                            (void *)&worker_args,
                                      (configMAX_PRIORITIES - 2),
                                          (xTaskHandle *)NULL  );

                if( result != pdPASS )
                {
                    CPU_SystemReset();
                }
#endif

                result = FRTOS1_xTaskCreate( vAppProcessHTTPTask,
                                                     "HTTPWorker",
                                        configMINIMAL_STACK_SIZE,
                                            (void *)&worker_args,
                                      (configMAX_PRIORITIES - 2),
                                          (xTaskHandle *)NULL  );

                if( result != pdPASS )
                {
                    CPU_SystemReset();
                }
                // to make sure this socket doesn't get stale CAN data, empty out its queue
                FRTOS1_xQueueReset( CAN_Msg_queue[indexToUse] );
                // Create the Tx task
                TCP_Tx_Thread_Args_t tx_args;
                tx_args.socket = socketIn[indexToUse];
                tx_args.socketIndex = indexToUse;
                tx_args.canMsgQueue = CAN_Msg_queue[indexToUse];

#if 0 //RB - Not sure if this is needed
                result = FRTOS1_xTaskCreate(    vAppTaskTcpTx,
                                                "TCPTxThread",
                              (configMINIMAL_STACK_SIZE + 50),
                                             (void *)&tx_args,
                                 ( configMAX_PRIORITIES - 2 ),
                               &( txTaskHandles[indexToUse] )  );

                if( pdPASS != result )
                {
                    CPU_SystemReset();
                }
#endif


                // inform the watchdog module that the worker and tx thread are now active
                //TODO vAppWatchdogUpdateTxTaskStatus( indexToUse, TRUE );
                //TODO vAppWatchdogUpdateWorkerTaskStatus( indexToUse, TRUE );
            }
        }
        else
        {
            // no free sockets at this time, kill some time
            FRTOS1_vTaskDelay( 100 );
        }

        // update the connection count variable
        uint8 conCnt = 0;
        for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
        {
            if( socketInActive[i] == 1 )
            {
                conCnt++;
            }
        }
        TCPConnectionCnt = conCnt;
    }

    // Task function does not return
}


static void vAppTaskTcpTx( void *pvParams )
{
    volatile uint32 *pu32TaskIsRunning;
    // Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
    pu32TaskIsRunning = (volatile uint32 *)pvParams;

//    CAN_Msg_And_Socket_t canMsgAndSock;
    uint8_t fwd[1 + 4 + 8] = { 0 };
    uint8_t fwdEcho[1 + 4 + 8] = { 0 };
    uint8_t cobs[128];
    uint8_t cobsEcho[128];
    uint8_t len = 0;
    uint8_t lenEcho = 0;
    uint8_t i = 0;
    uint32_t result = 0;

    // get our arguments 
    TCP_Tx_Thread_Args_t *arguments = (TCP_Tx_Thread_Args_t *)pvParams;
    Socket_t socket = arguments->socket;
    uint8 socketIndex = arguments->socketIndex;
    QueueHandle_t CANMsgQueue = arguments->canMsgQueue;

    // sanity check the index
    if( socketIndex >= TCP_MAX_CONNECTION_CNT )
    {
        // something has gone wrong
        CPU_SystemReset();
    }

    // update this socket's send timeout before we use it the first time
    TickType_t timeout = ( TCP_THREAD_TX_TIMEOUT );

    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_SNDTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        CPU_SystemReset();
    }

    for(;;)
    {
        // let the watchdog module know we're still running
        //TODO vAppWatchdogResetTCPTxTaskCtr( socketIndex );
        FRTOS1_vTaskDelay(200);

//        // wait up to 10ms for a new CAN message
//        if( FRTOS1_xQueueReceive( CANMsgQueue, &canMsgAndSock, app_MSEC_TO_TICK(10)) == pdPASS )
//        {
//            // we will assemble two messages.  if we are echoing the message back to the socket that
//            // sent it down to us, we add 0x10 to the first byte as a flag to the sender that this
//            // is the echo of their message
//            fwd[0] = canMsgAndSock.message.Length;
//            fwdEcho[0] = ( ( canMsgAndSock.message.Length ) + 0x10 );
//
//            if( canMsgAndSock.message.ID & CAN_ID_EXTENDED )
//            {
//                fwd[1] = fwdEcho[1] = (uint8) ( canMsgAndSock.message.ID >> 24 );
//                fwd[2] = fwdEcho[2] = (uint8) ( canMsgAndSock.message.ID >> 16 );
//                fwd[3] = fwdEcho[3] = (uint8) ( canMsgAndSock.message.ID >> 8 );
//                fwd[4] = fwdEcho[4] = (uint8) ( canMsgAndSock.message.ID >> 0 );
//
//                for( i = 0; i < canMsgAndSock.message.Length; i++ )
//                {
//                    fwd[i + 5] = fwdEcho[i + 5] = canMsgAndSock.message.Data[i];
//                }
//
//                len = COBS_TxBuffer( cobs, sizeof ( cobs ), ( 5 + canMsgAndSock.message.Length ), fwd );
//                lenEcho = COBS_TxBuffer( cobsEcho, sizeof ( cobsEcho ), ( 5 + canMsgAndSock.message.Length ), fwdEcho );
//            }
//            else
//            {
//                fwd[1] = fwdEcho[1] = (uint8) ( canMsgAndSock.message.ID >> 8 );
//                fwd[2] = fwdEcho[2] = (uint8) ( canMsgAndSock.message.ID >> 0 );
//
//                for( i = 0; i < canMsgAndSock.message.Length; i++ )
//                {
//                    fwd[i + 3] = fwdEcho[i + 3] = canMsgAndSock.message.Data[i];
//                }
//
//                len = COBS_TxBuffer( cobs, sizeof ( cobs ), ( 3 + canMsgAndSock.message.Length ), fwd );
//                lenEcho = COBS_TxBuffer( cobsEcho, sizeof ( cobsEcho ), ( 3 + canMsgAndSock.message.Length ), fwdEcho );
//            }
//
//            // send this message to this socket
//            uint8_t bytesToSend = 0;
//            uint8_t *txBuf;
//            int32_t bytesSentThisCall = 0;
//            int32_t bytesSentTotal = 0;
//            uint32_t inactiveTimeout = 0;
//
//            // determine which message to send
//            if( socketIndex == canMsgAndSock.socketIndex )
//            {
//                // use the echo message
//                bytesToSend = lenEcho;
//                txBuf = cobsEcho;
//            }
//            else
//            {
//                // use the normal message
//                bytesToSend = len;
//                txBuf = cobs;
//            }
//
//            while( bytesSentTotal < len )
//            {
//                // FreeRTOS_send will send as many bytes as possible, up to bytesToSend.  However, it may send
//                // less.  We need to track this and call _send again with the unsent data.
//                bytesSentThisCall = FreeRTOS_send( socket, & ( txBuf[bytesSentTotal] ), ( bytesToSend - bytesSentTotal ), 0 );
//                if( bytesSentThisCall >= 0 )
//                {
//                    bytesSentTotal += bytesSentThisCall;
//                }
//                else
//                {
//                    // Error transmitting, abort this message
//                    break;
//                }
//
//                if( bytesSentThisCall == 0 )
//                {
//                    // monitor for a dead connection
//                    inactiveTimeout++;
//
//                    if( inactiveTimeout >= TCP_THREAD_TX_IDLE_TIMEOUT )
//                    {
//                        // abort this message
//                        break;
//                    }
//                }
//                else
//                {
//                    inactiveTimeout = 0;
//                }
//            }
//        }
    }

    // Task function does not return
}



//
//static void vSendMsgToCANQueues( CAN_Msg_And_Socket_t *msg )
//{
//    uint8_t i = 0;
//
//    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
//    {
//        // only add a message to this queue if its socket is active
//        if( socketInActive[i] == 1 )
//        {
//            if( FRTOS1_xQueueSendToBack(CAN_Msg_queue[i], msg, 0) != pdTRUE )
//            {
//                u32NetQueueErrCnt++;
//            }
//        }
//    }
//}
//



static void TCP_Process_Special_Msg( const uint8 * message )
{
    switch( message[0] )
    {
        case 0xFB :
            CPU_SystemReset();
            break;

        default :
            break;
    }
}


static void vAppTaskTcpWorker( void *pvParams )
{
    typedef struct
    {
        uint8 Data[64];
    } SOCKETBUF;
    SOCKETBUF RxBuffer[1];
#define RECV_BUF_SIZE 128
    uint8_t recvData[RECV_BUF_SIZE];
    int32_t recvBytes;
    uint32_t i = 0;
    TickType_t timeout;
    uint32_t inactive = 0;
    uint32_t result = 0;

    TCP_Worker_Thread_Args_t *arguments = (TCP_Worker_Thread_Args_t *)pvParams;
    Socket_t socket = arguments->socket;
    uint8 socketIndex = arguments->socketIndex;
    QueueHandle_t responseQueue = arguments->responseQueue;

    const uint8 * message = NULL;

    timeout = ( TCP_THREAD_RX_TIMEOUT );
    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_RCVTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        CPU_SystemReset();
    }

    do
    {
        recvBytes = FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 );

        if( recvBytes == 0 )
        {
            // no activity received, increment our timeout variable
            inactive++;

            if( inactive >= TCP_THREAD_RX_IDLE_TIMEOUT )
            {
                // close this socket
                break;
            }
        }
        else if( recvBytes > 0 )
        {
            // activity received
            inactive = 0;
        }

    } while( ( recvBytes >= 0 ) && ( socketIndex < TCP_MAX_CONNECTION_CNT ) ); // exit loop on error (negative value returned by FreeRTOS_recv() )
                                                                               // or if something happens to the socketIndex value that puts it out of range

    /* Close connection and discard connection identifier. */
    FreeRTOS_shutdown( socket, FREERTOS_SHUT_RDWR );

    uint32_t shutdownTimeout = 0;

    while( FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 ) >= 0 )
    {
        /* Wait for shutdown to complete.  If a receive block time is used then
         this delay will not be necessary as FreeRTOS_recv() will place the RTOS task
         into the Blocked state anyway. */
        FRTOS1_vTaskDelay( 10 );

        /* Note - real applications should implement a timeout here, not just
         loop forever. */

        // if the socket has not shutdown after a timeout, force it closed
        shutdownTimeout++;
        if( shutdownTimeout >= TCP_THREAD_RX_SHUTDOWN_TIMEOUT )
        {
            break;
        }
    }

    FreeRTOS_closesocket( socket );

    // inform the rx thread that we are closing this socket
    xSocketConnectionInfoType msg;
    msg.socket = socket;
    msg.index = socketIndex;
    msg.active = 0;

    FRTOS1_xQueueSendToBack( responseQueue, &msg, 0 );

    // delete this thread (the parameter NULL means delete the task that called FRTOS1_vTaskDelete)
}




static void vAppProcessHTTPTask( void *pvParams )
{
#define RECV_BUF_SIZE 128
    uint8_t recvData[RECV_BUF_SIZE];
    int32_t recvBytes;
    TickType_t timeout;
    uint32_t inactive = 0;
    uint32_t result = 0;
    uint8_t bytesToSend = 0;
    int32_t bytesSentThisCall = 0;
    int32_t bytesSentTotal = 0;
    uint32_t inactiveTimeout = 0;
    uint8_t len = 0;

    //char httpText[] = "Hello World";
    char httpText[] = "<HTML><BODY> TESTER SW REV G </BODY></HTML>";

    TCP_Worker_Thread_Args_t *arguments = (TCP_Worker_Thread_Args_t *)pvParams;
    Socket_t socket = arguments->socket;
    uint8 socketIndex = arguments->socketIndex;
    QueueHandle_t responseQueue = arguments->responseQueue;

    timeout = ( TCP_THREAD_RX_TIMEOUT );
    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_RCVTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        CPU_SystemReset();
    }
    do
    {
        recvBytes = FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 );

        /* TODO: Do something with the received data */
        if( recvBytes == 0 )
        {
            // no activity received, increment our timeout variable
            inactive++;

            if( inactive >= TCP_THREAD_RX_IDLE_TIMEOUT )
            {
                // close this socket
                break;
            }
        }
        else if( recvBytes > 0 )
        {
            // activity received
            inactive = 0;

            len = sizeof(httpText);
            bytesToSend = len;
            while( bytesSentTotal < len )
            {
                // FreeRTOS_send will send as many bytes as possible, up to bytesToSend.  However, it may send
                // less.  We need to track this and call _send again with the unsent data.
                bytesSentThisCall = FreeRTOS_send( socket, & ( httpText[bytesSentTotal] ), ( bytesToSend - bytesSentTotal ), 0 );
                if( bytesSentThisCall >= 0 )
                {
                    bytesSentTotal += bytesSentThisCall;
                }
                else
                {
                    // Error transmitting, abort this message
                    break;
                }

                if( bytesSentThisCall == 0 )
                {
                    // monitor for a dead connection
                    inactiveTimeout++;

                    if( inactiveTimeout >= TCP_THREAD_TX_IDLE_TIMEOUT )
                    {
                        // abort this message
                       break;
                    }
                }
                else
                {
                    inactiveTimeout = 0;
                }
            }

        }

    } while( ( recvBytes >= 0 ) && ( socketIndex < TCP_MAX_CONNECTION_CNT ) ); // exit loop on error (negative value returned by FreeRTOS_recv() )
                                                                               // or if something happens to the socketIndex value that puts it out of range
    /* Close connection and discard connection identifier. */
    FreeRTOS_shutdown( socket, FREERTOS_SHUT_RDWR );

    uint32_t shutdownTimeout = 0;

    while( FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 ) >= 0 )
    {
        /* Wait for shutdown to complete.  If a receive block time is used then
         this delay will not be necessary as FreeRTOS_recv() will place the RTOS task
         into the Blocked state anyway. */
        FRTOS1_vTaskDelay( 10 );

        /* Note - real applications should implement a timeout here, not just
         loop forever. */

        // if the socket has not shutdown after a timeout, force it closed
        shutdownTimeout++;
        if( shutdownTimeout >= TCP_THREAD_RX_SHUTDOWN_TIMEOUT )
        {
            break;
        }
    }

    FreeRTOS_closesocket( socket );

    // inform the rx thread that we are closing this socket
    xSocketConnectionInfoType msg;
    msg.socket = socket;
    msg.index = socketIndex;
    msg.active = 0;

    FRTOS1_xQueueSendToBack( responseQueue, &msg, 0 );

    // delete this thread (the parameter NULL means delete the task that called FRTOS1_vTaskDelete)
    vTaskDelete(NULL);
}



static void vAppTaskUdpBcast( void *pvParams )
{
    volatile uint32_t *pu32TaskIsRunning;

    Socket_t udpSocket;
    struct freertos_sockaddr udpDestinationAddress;
    uint8_t broadcastMsg[200] = "";

    // set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
    pu32TaskIsRunning = (volatile uint32_t *)pvParams;

    udpDestinationAddress.sin_port = FreeRTOS_htons( UDP_PORT_NUM );

    /* Create the socket. */
    udpSocket = FreeRTOS_socket( FREERTOS_AF_INET,
                FREERTOS_SOCK_DGRAM, /*FREERTOS_SOCK_DGRAM for UDP.*/
                FREERTOS_IPPROTO_UDP );

    for(;;)
    {
        // this will set the destination address to the broadcast address for the network
        udpDestinationAddress.sin_addr = FreeRTOS_GetIPAddress() | ~ ( FreeRTOS_GetNetmask() );

        broadcastMsg[0] = '\0';
        sprintf( broadcastMsg, "{\"mfg\":\"%s\", \"product\":\"%s\", \"name\":\"%s\", \"port\":\"%d\" }", UDP_MFG_STRING,
                    UDP_PRODUCT_STRING, "Demo", TCP_PORT_NUM );

        FreeRTOS_sendto( udpSocket,
                    broadcastMsg,
                    strlen( broadcastMsg ),
                    0,
                    &udpDestinationAddress,
                    sizeof ( udpDestinationAddress ) );

        // Update our watchdog variable
        portENTER_CRITICAL();
        *pu32TaskIsRunning = TRUE;
        portEXIT_CRITICAL();

        FRTOS1_vTaskDelay( UDP_BROADCAST_PERIOD );
    }

    // task function does not return
}


