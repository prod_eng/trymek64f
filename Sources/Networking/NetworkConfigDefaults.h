/**---------------------------------------------------------------------------------------------------------------------
 * @file            NetworkConfigDefaults.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Apr 17, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_NETWORKING_NETWORKCONFIGDEFAULTS_H_
#define SOURCES_NETWORKING_NETWORKCONFIGDEFAULTS_H_


// TCP/UDP Network parameters
#define TCP_MAX_CONNECTION_CNT              4

#define TCP_THREAD_RX_TIMEOUT             100   // Wait up to 100ms at a time for RX messages
#define TCP_THREAD_RX_IDLE_TIMEOUT         50   // The number of consecutive RX_TIMEOUTS before a thread is considered inactive
#define TCP_THREAD_RX_SHUTDOWN_TIMEOUT    300   // The number of consecutive RX_TIMEOUTS to wait while a socket is shutting down
#define TCP_THREAD_TX_TIMEOUT              10   // Wait up to 10ms at a time for TX messages
#define TCP_THREAD_TX_IDLE_TIMEOUT          5   // The number of consecutive TX_TIMEOUTS before a message TX is aborted

#define HARDCODED_IP_ADDRESS { 192, 168,   1,   1 }
#define HARDCODED_NET_MASK   { 255, 255, 255,   0 }
#define HARDCODED_GATEWAY    { 192, 168,   1,   1 }
#define HARDCODED_DNS_SERV   { 208,  67, 222, 222 }

#define IDS_MAC_PREFIX_1                0xD8
#define IDS_MAC_PREFIX_2                0xD7
#define IDS_MAC_PREFIX_3                0x23

#define TCP_PORT_NUM                    6969
#define UDP_PORT_NUM                    47664

#define UDP_BROADCAST_PERIOD            1000        // Period in milliseconds of the udp broadcast
#define UDP_MFG_STRING                  "IDS"
#define UDP_PRODUCT_STRING              "PRODUCTION_TESTER"



#endif // SOURCES_NETWORKING_NETWORKCONFIGDEFAULTS_H_
