/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppWebPages.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Jun 22, 2018
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
static const uint8 strHtmlPageHeader[] = "<HTML><HEAD><TITLE>IDS-Electronics Universal Tester</TITLE></HEAD><BODY>";
static const uint8 strHtmlPageFooter[] = "</BODY></HTML>";

// TODO keep this as large as is practical for now while we test to see what we'll actually use
static uint8 gstrHttpPageText[1000];

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
uint8 *pu8WebGeneratePage( eWEB_PAGE_TO_BUILD_TYPE ePageToBuild, uint32 *pNewLen )
{
    if( ePageToBuild < eWEB_PAGE_TO_BUILD_COUNT )
    {
        strcpy( gstrHttpPageText, strHtmlPageHeader );

        switch( ePageToBuild )
        {
            case eWEB_PAGE_TO_BUILD_VERSIONS :
                strcat( gstrHttpPageText, DESCRIPTION );
                strcat( gstrHttpPageText, "\n" );
                strcat( gstrHttpPageText, PART_NUMBER );
                break;

            default:
                strcat( gstrHttpPageText, "Invalid Page\n" );
                break;
        }

        strcat( gstrHttpPageText, strHtmlPageFooter );

        *pNewLen = strlen( gstrHttpPageText );
    }

    return &gstrHttpPageText[0];
}




/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */

/*
 * AppWebPages.c
 *
 *  Created on: Jun 22, 2018
 *      Author: mstroven
 */


