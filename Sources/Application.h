/**---------------------------------------------------------------------------------------------------------------------
 * @file            Application.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 3, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPLICATION_H_
#define SOURCES_APPLICATION_H_

#define     CPU_MK64FN1M0xxx12
#define     __LITTLE_ENDIAN__

// DEBUGGING AND TESTING MACROS:
//#define     WATCHDOG_DISABLE
//#define     DEBUG_CAN_RX
//#define     DEBUG_CAN_TX


//#define     INCLUDE_IDS_CORE_CAN
#define     INCLUDE_ETHERNET_SUPPORT
#define     INCLUDE_RTC_SUPPORT
#define     USE_COBS_FOR_HOST_SERIAL_TRAFFIC
#define     DEBUG_HOST_COMM

// Application Constants
#define REVISION "A"
#define PART_NUMBER "00000-" REVISION
#define DESCRIPTION "LCI ETHERNET DEMO FW"


#ifdef INCLUDE_ETHERNET_SUPPORT
#define UseDHCPDefault      { 0x00 }
#define MACSuffixDefault    { 0x00, 0x00, 0x00 }
#define UDPNameDefault      { 'T', 'e', 's', 't', 'e', 'r', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' }
#endif // INCLUDE_ETHERNET_SUPPORT




// Global PEx includes
#include "Cpu.h"
#include "Events.h"
#include "Pins1.h"
#include "FRTOS1.h"
#include "UTIL1.h"
#include "MCUC1.h"
#include "SYS1.h"
#include "WAIT1.h"
#include "XF1.h"
#include "CS1.h"
#include "AS1.h"
#include "ASerialLdd1.h"
#include "RTT1.h"
#include "CLS1.h"
#include "Bit1.h"
#include "BitIoLdd1.h"
#include "ETH0.h"
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PDD_Includes.h"
#include "Init_Config.h"


#include <stdio.h>
#include <string.h>



// IDS-Core functionality
#ifdef INCLUDE_IDS_CORE_CAN
  #include "AppIDSCoreModule.h"
  #include "AppMyRVLogic.h"
  #include "MyRVDeviceIntegration.h"
  #include "MyRVDeviceMfg.h"
#endif // INCLUDE_IDS_CORE_CAN

#ifdef INCLUDE_ETHERNET_SUPPORT
// Ethernet
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_DHCP.h"
#include "FreeRTOS_IP_Private.h"
#include "AppEthernetModule.h"
#include "AppWebPages.h"
#else
  void vEthernetSignalDHCPFailed(void); // Stub
#endif // INCLUDE_ETHERNET_SUPPORT

// Watchdog
//#include "AppWatchdogModule.h"



typedef uint8 HAL_ETHERNET_MAC_ADDR_TYPE;

// APLLICATION CUSTOM TASK API
typedef void (* const PFN_V_U8_U16)(uint8, uint16 );    // Function Pointer for functions that accept a uint8 and a uint16 but don't return a value
typedef bool (* const PFN_BO_V)(void);                  // Function Pointer for functions that don't take a parameter, and return a bool
typedef void (* const PFN_V_V)(void);                   // Function Pointer for functions that don't take a parameter, and don't return a value

typedef struct xTaskApiStructType
{
    PFN_V_U8_U16 pfnInit; /* Init contains priority and stack size */
    PFN_BO_V pfnWdgChk;
    PFN_V_V pfnWdgClear;
    uint8 u8Priority;
    uint16 u16StackSize;
}xTASK_API_STRUCT_TYPE;


typedef enum eApplicationTaskType
{
    //eAPPLICATION_TASK_ANALOG = 0,
    eAPPLICATION_TASK_CAN_RX = 0,
    eAPPLICATION_TASK_CAN_TX,
    eAPPLICATION_TASK_CAN_EXTRA_RX,
    eAPPLICATION_TASK_CAN_EXTRA_TX,
    eAPPLICATION_TASK_I2C,
    eAPPLICATION_TASK_SPI,
    eAPPLICATION_TASK_HOST_COMM,
    eAPPLICATION_TASK_DUT_COMM,
    eAPPLICATION_TASK_CMD_PROC,
    eAPPLICATION_TASK_ANA_MEASURE,

#ifdef SEPARATE_TASK_FOR_RF_TX
    eAPPLICATION_TASK_RF_TX,
#endif // SEPARATE_TASK_FOR_RF_TX

#ifdef INCLUDE_IDS_CORE_CAN
    eAPPLICATION_TASK_IDS_CORE,
#endif // INCLUDE_IDS_CORE_CAN

#ifdef INCLUDE_RTC_SUPPORT
    eAPPLICATION_TASK_RTC,
#endif // INCLUDE_RTC_SUPPORT

#ifdef INCLUDE_ETHERNET_SUPPORT
    eAPPLICATION_TASK_ETHERNET_RX,
    eAPPLICATION_TASK_ETHERNET_TX,
#endif // INCLUDE_ETHERNET_SUPPORT

    eAPPLICATION_TASK_LCD_DISPLAY,
    eAPPLICATION_TASK_WATCHDOG,

    // New Tasks above this line
    eAPPLICATION_TASK_COUNT
}eAPPLICATION_TASK_TYPE;

void vApplicationInitTasks(void);
void vApplication_OnIDSCanMessageRx( const void *xIdsCanMsg );
void vApplication_OnIDSCanMessageTx( const void *xIdsCanMsg );

void vAppDebugPrintAnnouncement(void);
const uint8 *pApplicationAskSoftwarePartnum(void);

void vDoNothingFn(void);
bool boDoNothingFn(void);

bool boWatchdogCheckAllOtherTasks(void);
void vWatchdogClearTaskWdgFlags(void);
void vLongEndianSwap( uint32 *pLongToSwap );
uint32 Random32(void);

#define FOREVER  while(1)

#endif // SOURCES_APPLICATION_H_
