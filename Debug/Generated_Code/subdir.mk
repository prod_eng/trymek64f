################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Generated_Code/AS1.c \
../Generated_Code/ASerialLdd1.c \
../Generated_Code/Bit1.c \
../Generated_Code/BitIoLdd1.c \
../Generated_Code/CLS1.c \
../Generated_Code/CS1.c \
../Generated_Code/Cpu.c \
../Generated_Code/ETH0.c \
../Generated_Code/FRTOS1.c \
../Generated_Code/MCUC1.c \
../Generated_Code/PE_LDD.c \
../Generated_Code/Pins1.c \
../Generated_Code/RTT1.c \
../Generated_Code/RTT_Syscalls_GCC.c \
../Generated_Code/SEGGER_RTT.c \
../Generated_Code/SEGGER_RTT_printf.c \
../Generated_Code/SEGGER_SYSVIEW.c \
../Generated_Code/SEGGER_SYSVIEW_Config.c \
../Generated_Code/SEGGER_SYSVIEW_FreeRTOS.c \
../Generated_Code/SYS1.c \
../Generated_Code/UTIL1.c \
../Generated_Code/WAIT1.c \
../Generated_Code/XF1.c \
../Generated_Code/croutine.c \
../Generated_Code/event_groups.c \
../Generated_Code/heap_1.c \
../Generated_Code/heap_2.c \
../Generated_Code/heap_3.c \
../Generated_Code/heap_4.c \
../Generated_Code/heap_5.c \
../Generated_Code/heap_useNewlib.c \
../Generated_Code/list.c \
../Generated_Code/mpu_wrappers.c \
../Generated_Code/port.c \
../Generated_Code/queue.c \
../Generated_Code/stream_buffer.c \
../Generated_Code/tasks.c \
../Generated_Code/timers.c 

OBJS += \
./Generated_Code/AS1.o \
./Generated_Code/ASerialLdd1.o \
./Generated_Code/Bit1.o \
./Generated_Code/BitIoLdd1.o \
./Generated_Code/CLS1.o \
./Generated_Code/CS1.o \
./Generated_Code/Cpu.o \
./Generated_Code/ETH0.o \
./Generated_Code/FRTOS1.o \
./Generated_Code/MCUC1.o \
./Generated_Code/PE_LDD.o \
./Generated_Code/Pins1.o \
./Generated_Code/RTT1.o \
./Generated_Code/RTT_Syscalls_GCC.o \
./Generated_Code/SEGGER_RTT.o \
./Generated_Code/SEGGER_RTT_printf.o \
./Generated_Code/SEGGER_SYSVIEW.o \
./Generated_Code/SEGGER_SYSVIEW_Config.o \
./Generated_Code/SEGGER_SYSVIEW_FreeRTOS.o \
./Generated_Code/SYS1.o \
./Generated_Code/UTIL1.o \
./Generated_Code/WAIT1.o \
./Generated_Code/XF1.o \
./Generated_Code/croutine.o \
./Generated_Code/event_groups.o \
./Generated_Code/heap_1.o \
./Generated_Code/heap_2.o \
./Generated_Code/heap_3.o \
./Generated_Code/heap_4.o \
./Generated_Code/heap_5.o \
./Generated_Code/heap_useNewlib.o \
./Generated_Code/list.o \
./Generated_Code/mpu_wrappers.o \
./Generated_Code/port.o \
./Generated_Code/queue.o \
./Generated_Code/stream_buffer.o \
./Generated_Code/tasks.o \
./Generated_Code/timers.o 

C_DEPS += \
./Generated_Code/AS1.d \
./Generated_Code/ASerialLdd1.d \
./Generated_Code/Bit1.d \
./Generated_Code/BitIoLdd1.d \
./Generated_Code/CLS1.d \
./Generated_Code/CS1.d \
./Generated_Code/Cpu.d \
./Generated_Code/ETH0.d \
./Generated_Code/FRTOS1.d \
./Generated_Code/MCUC1.d \
./Generated_Code/PE_LDD.d \
./Generated_Code/Pins1.d \
./Generated_Code/RTT1.d \
./Generated_Code/RTT_Syscalls_GCC.d \
./Generated_Code/SEGGER_RTT.d \
./Generated_Code/SEGGER_RTT_printf.d \
./Generated_Code/SEGGER_SYSVIEW.d \
./Generated_Code/SEGGER_SYSVIEW_Config.d \
./Generated_Code/SEGGER_SYSVIEW_FreeRTOS.d \
./Generated_Code/SYS1.d \
./Generated_Code/UTIL1.d \
./Generated_Code/WAIT1.d \
./Generated_Code/XF1.d \
./Generated_Code/croutine.d \
./Generated_Code/event_groups.d \
./Generated_Code/heap_1.d \
./Generated_Code/heap_2.d \
./Generated_Code/heap_3.d \
./Generated_Code/heap_4.d \
./Generated_Code/heap_5.d \
./Generated_Code/heap_useNewlib.d \
./Generated_Code/list.d \
./Generated_Code/mpu_wrappers.d \
./Generated_Code/port.d \
./Generated_Code/queue.d \
./Generated_Code/stream_buffer.d \
./Generated_Code/tasks.d \
./Generated_Code/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Generated_Code/%.o: ../Generated_Code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/System" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/PDD" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/IO_Map" -I"/Applications/KDS_3.0.0.app/Contents/eclipse/ProcessorExpert/lib/Kinetis/pdd/inc" -I"/Users/mstroven/git_repos/TryThisK64F/Sources" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/PHY" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/include" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/Compiler/GCC" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/NetworkInterface" -I"/Users/mstroven/git_repos/TryThisK64F/Generated_Code" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


