################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Static_Code/System/CPU_Init.c \
../Static_Code/System/Peripherals_Init.c \
../Static_Code/System/Vectors.c 

OBJS += \
./Static_Code/System/CPU_Init.o \
./Static_Code/System/Peripherals_Init.o \
./Static_Code/System/Vectors.o 

C_DEPS += \
./Static_Code/System/CPU_Init.d \
./Static_Code/System/Peripherals_Init.d \
./Static_Code/System/Vectors.d 


# Each subdirectory must supply rules for building sources it contributes
Static_Code/System/%.o: ../Static_Code/System/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/System" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/PDD" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/IO_Map" -I"/Applications/KDS_3.0.0.app/Contents/eclipse/ProcessorExpert/lib/Kinetis/pdd/inc" -I"/Users/mstroven/git_repos/TryThisK64F/Sources" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/PHY" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/include" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/Compiler/GCC" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/NetworkInterface" -I"/Users/mstroven/git_repos/TryThisK64F/Generated_Code" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


