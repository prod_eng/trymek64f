################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_driver.c \
../Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_hal.c \
../Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/NetworkInterface.c 

OBJS += \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_driver.o \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_hal.o \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/NetworkInterface.o 

C_DEPS += \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_driver.d \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/ETH_hal.d \
./Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/NetworkInterface.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/%.o: ../Sources/Networking/FreeRTOS-Plus-TCP/portable/NetworkInterface/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/System" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/PDD" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/IO_Map" -I"/Applications/KDS_3.0.0.app/Contents/eclipse/ProcessorExpert/lib/Kinetis/pdd/inc" -I"/Users/mstroven/git_repos/TryThisK64F/Sources" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/PHY" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/include" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/Compiler/GCC" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/NetworkInterface" -I"/Users/mstroven/git_repos/TryThisK64F/Generated_Code" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


