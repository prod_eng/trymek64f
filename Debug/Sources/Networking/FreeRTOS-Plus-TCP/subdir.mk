################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_ARP.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DHCP.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DNS.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_IP.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Sockets.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.c \
../Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.c 

OBJS += \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_ARP.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DHCP.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DNS.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_IP.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Sockets.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.o \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.o 

C_DEPS += \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_ARP.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DHCP.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_DNS.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_IP.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Sockets.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.d \
./Sources/Networking/FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/Networking/FreeRTOS-Plus-TCP/%.o: ../Sources/Networking/FreeRTOS-Plus-TCP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/System" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/PDD" -I"/Users/mstroven/git_repos/TryThisK64F/Static_Code/IO_Map" -I"/Applications/KDS_3.0.0.app/Contents/eclipse/ProcessorExpert/lib/Kinetis/pdd/inc" -I"/Users/mstroven/git_repos/TryThisK64F/Sources" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/PHY" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/include" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/Compiler/GCC" -I"/Users/mstroven/git_repos/TryThisK64F/Sources/Networking/FreeRTOS-Plus-TCP/Portable/NetworkInterface" -I"/Users/mstroven/git_repos/TryThisK64F/Generated_Code" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


